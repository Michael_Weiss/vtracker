let sendButton = $('#send-btn');
let leaveButton = $('#leave-btn');
let helpButton = $('#help-btn');
let textInput = $('#msg-input');


function drawWord(word, user){
    //
    const wordItem = `<li class="list-group-item">${word}</li>`;
    if (currentUser === user){
        $(wordItem).appendTo('#my-words');
    } else {
        $(wordItem).appendTo('#not-my-words');
    }
}

function changeLed(user, with_word=false){
    if (currentUser !== user && with_word === false){
        document.getElementById('led').style.backgroundColor = 'red';
    } else {
        document.getElementById('led').style.backgroundColor = 'gray';
    }
}

function changeUserElement(username){
    document.getElementById('opponent').style.backgroundColor = '#b8daff';
    document.getElementById('opponent').style.color = '#004085';
    document.getElementById('opponent').innerHTML = username;
}

function disableInput(){
    document.getElementById("send-btn").disabled = true;
    document.getElementById("msg-input").disabled = true;
}

function enableInput(){
    document.getElementById("send-btn").disabled = false;
    document.getElementById("msg-input").disabled = false;
}



$(document).ready(function () {

    disableInput();

    const roomID = JSON.parse(document.getElementById('room-id').textContent);

    // create new Web Socket channel to communicate with server
    const socket = new WebSocket(
        'ws://'
        + window.location.host
        + '/ws/game/'
        + roomID
        + '/'
    );

    if (typeof first_user !== 'undefined'){
        changeUserElement(first_user);
        enableInput();
    }

    textInput.keypress(function (e) {
        if (e.keyCode === 13)
            sendButton.click();
    });

    sendButton.click(function () {
        socket.send(textInput.val());
    });

    helpButton.click(function () {
        alert('The idea of the game is that each of the players utters a randomly chosen word.\n' +
            ' Then the goal of the players is to come to the same word with the help of consecutive associations.\n' +
            ' Both utter a word one way or another connected with the previous two and so on until the desired coincidence happens.');
    });

    leaveButton.click(function () {
        window.location.replace('/game/lobby/');
    });

    // when new message is received - do something.
    socket.onmessage = function (e) {
        let info = JSON.parse(e.data);
        if (info.status === 'word'){
            console.log('Word was received.');
            drawWord(info.word, info.user);
            changeLed(info.user, true);
        } else if (info.status === 'first_word'){
            // draw hidden word.
            console.log('First word was received.');
            if (info.user === currentUser){
                console.log('Draw first word...');
                drawWord(info.word, '');
            }
        } else if (info.status === 'repeat'){
            console.log('Warning was received. Status = "Repeat"');
            alert('This word was already in use.')
        } else if (info.status === 'new_user'){
            console.log('User enter the game.');
            if (info.user !== currentUser){
                changeUserElement(info.user);
                enableInput();
            }
            document.getElementById("msg-input").focus();
        } else if (info.status === 'end'){
            console.log('Win!');
            alert('Win!');
            disableInput();
        } else if (info.status === 'wait'){
            console.log('Warning was received. Status = "Wait".');
            alert('Wait your turn, ' + info.user + '.');
        } else if (info.status === 'signal'){
            // draw word if signal was from current user.
            if (info.user === currentUser){
                console.log('Signal was received from you.');
                drawWord(textInput.val(), currentUser);
            } else {
                console.log('Signal was received from another user.');
                changeLed(info.user);
            }
        } else if (info.status === 'disconnect'){
            console.log('Another user is disconnected.');
            alert('User has left the game.');
            setTimeout(function(){ alert("User has left the game."); }, 3000);
            window.location.replace('/game/lobby/');
        }
    };

    socket.onclose = function () {
        console.log('Websocket was closed.');
    }
});