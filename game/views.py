from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect
from .models import GameModel
from django.db.models import Count


def game(request, room_id):
    """"""
    try:
        room = GameModel.objects.get(id=room_id)
    except GameModel.DoesNotExist:
        return render(request, 'game_lobby.html', {'message': 'This room does not exist.'})
    users = room.gamers
    if users.count() == 0:
        room.delete()
        raise PermissionDenied()
    elif users.count() == 1:
        context = {'room_id': room_id}
        # if new user enter the room
        if room.gamers.last() != request.user:
            context['first_user'] = room.gamers.last().username
            room.gamers.add(request.user.id)
            room.save()
        return render(request, 'game.html', context)
    else:
        return render(request, 'game_lobby.html', {'message': 'This room is already full.'})


def game_lobby(request):
    """"""
    context = {}
    qs = GameModel.objects.annotate(num_gamers=Count('gamers')).filter(num_gamers=1)
    if qs.count() > 0:
        context['games'] = qs
    return render(request, 'game_lobby.html', context)


def game_create(request):
    """"""
    room = GameModel.objects.create()
    room.gamers.add(request.user.id)
    room.save()
    return redirect('game-room', room_id=room.pk)
