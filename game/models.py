from asgiref.sync import async_to_sync
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import m2m_changed
from channels.layers import get_channel_layer
from django.dispatch import receiver


class GameModel(models.Model):
    """"""
    gamers = models.ManyToManyField(User, blank=True)
    tries = models.IntegerField(null=True, blank=True)
    finished = models.BooleanField(default=False)


@receiver(m2m_changed, sender=GameModel.gamers.through)
def enter_game(sender, **kwargs):
    """"""
    action = kwargs.pop('action', None)
    if action == "post_add":
        channel_layer = get_channel_layer()
        user = kwargs['instance'].gamers.last()
        notification = {'type': 'send_signal', 'status': 'new_user', 'user': user.username}
        async_to_sync(channel_layer.group_send)(f"{kwargs['instance'].id}", notification)

