from django.contrib.auth.decorators import login_required
from django.urls import path
from .views import game, game_create, game_lobby


urlpatterns = [
    path('<int:room_id>/', login_required(game, login_url='/accounts/login/'), name='game-room'),
    path('create/', login_required(game_create, login_url='/accounts/login/'), name='game-create'),
    path('lobby/', login_required(game_lobby, login_url='/accounts/login/'), name='game-lobby'),
]
