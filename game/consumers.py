from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.core.cache import cache
import json

from game.models import GameModel


def set_game_results(game_id, words_num):
    """Set game win_flag to 'true' and write number of tries."""
    game = GameModel.objects.get(id=game_id)
    if isinstance(words_num, int) and (words_num % 2) == 0:
        game.tries = words_num/2
    game.finished = True
    game.save()


class GameConsumer(WebsocketConsumer):
    """Consumer for game."""

    def connect(self):
        """If new connection is established - add channel to group."""
        self.group_name = self.scope['url_route']['kwargs']['game_name']
        async_to_sync(self.channel_layer.group_add)(self.group_name, self.channel_name)
        self.accept()

    def disconnect(self, code):
        """If disconnected - remove channel name from group."""
        async_to_sync(self.channel_layer.group_discard)(self.group_name, self.channel_name)
        signal = {"type": "send_signal", 'status': 'disconnect', 'user': ''}
        async_to_sync(self.channel_layer.group_send)(self.group_name, signal)
        try:
            instance = GameModel.objects.get(id=self.group_name)
            if instance.finished is False:
                instance.delete()
        except Exception as exc:
            pass

    def receive(self, text_data=None, bytes_data=None):
        """Receive message from client and process it."""
        try:
            # get words from cache (if any), associated with room id .
            words = cache.get(self.group_name)

            # if no words was in this game.
            if words is None:
                # set cache to array.
                cache.set(self.group_name, [''], timeout=None)
                words = cache.get(self.group_name)
                self.check_word(text_data, words)

            # if user tries to send new messages but game is finished --> send 'end' signal.
            elif len(words) > 2 and words[-2:][0].lower() == words[-2:][1].lower():
                self.send(text_data=json.dumps({
                    'status': 'end',
                }))

            # if list with words in cache and last 2 words are different --> process word.
            elif isinstance(words, list):
                self.check_word(text_data, words)

        except Exception as exc:
            pass

    def check_word(self, text_data, data):
        """check received word."""
        username = self.scope['user'].username

        # if word is second (2) and from another user.
        if (len(data) % 2) == 0 and data[0] != username:
            # if words (1, 2) are same.
            if text_data.lower() == data[-2:][1].lower():
                # add word (2) and end this game.
                data.append(text_data)
                data[0] = username
                cache.set(self.group_name, data, timeout=None)
                set_game_results(self.group_name, len(data) - 1)
                signal = {"type": "send_signal", 'status': 'end', 'user': username}
                async_to_sync(self.channel_layer.group_send)(self.group_name, signal)

            # if words are different.
            else:
                # add word (2) and continue this game.
                data.append(text_data)
                data[0] = username
                cache.set(self.group_name, data, timeout=None)

            # send first (1) word to second's (2) word user.
            notification = {"type": "send_word", 'word': data[-2:][0], 'status': 'first_word', 'user': username}
            async_to_sync(self.channel_layer.group_send)(self.group_name, notification)
            # send second (2) word to both (1, 2) users.
            notification = {"type": "send_word", 'word': text_data, 'status': 'word', 'user': username}
            async_to_sync(self.channel_layer.group_send)(self.group_name, notification)

        # if user has already entered the word
        elif (len(data) % 2) == 0 and data[0] == username:
            # send error. Wait your turn.
            self.send(text_data=json.dumps({
                'status': 'wait',
                'user': username,
            }))

        # if word is first (1).
        else:  # (len(data) % 1) == 0

            # if word (1) in list.
            if text_data in data:
                # send error. Write another word.
                self.send(text_data=json.dumps({
                    'status': 'repeat',
                    'user': username,
                }))

            # if word (1) not in list.
            else:
                # add word (1) and send signal to users (1, 2).
                data.append(text_data)
                data[0] = username
                cache.set(self.group_name, data, timeout=None)
                signal = {"type": "send_signal", 'status': 'signal', 'user': username}
                async_to_sync(self.channel_layer.group_send)(self.group_name, signal)

    def send_word(self, event):
        """Send word to all room members."""
        word = event['word']
        status = event['status']
        user = event['user']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'word': word,
            'status': status,
            'user': user,
        }))

    def send_signal(self, event):
        """Send signal without word to all room members."""
        status = event['status']
        user = event['user']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'status': status,
            'user': user,
        }))
