# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [] - Begin of internship:
### Added
- Ability to assign tasks to someone else
- Add the button in navbar, which opens a link with tasks that have been assigned to others (*)
### Changed
- Model-Based Views
### Fixed
- Fix edit-preview-edit (status disappears) issue
- Date fields issue
- Fields: second field for own tasks, second field for assigned tasks (*)
- Fix password compare issue


## [] - 2021-03-16
- If authenticated --> show name of current user
- Issue: can't sort tasks by status without date range
- Issue: "this field is required" after loading homepage
- Issue: can't logout from create page
- Ability to edit/delete tasks from home page, that was created from authorised user
- Issue: anyone can edit tasks via URL (poorly)
- Issue delete can everyone
issue: when go back after task creation and add again - create two tasks
paginate feature
  
## [] - 2021-03-19
- Try to optimize filter logic with Query Utils (Q from django.db.models), it is can help you to filter posts with no repeating of code
- In additional object collect posts that was created by current user
- Add logic to change status of task if current user is person to whose assigned task (poorly)
- Add logic to view post by a click on it

## [] - 2021-03-19
- Optimize filter logic with Query Utils (Q from django.db.models)
- Add comment functionality (*)

## [] - 2021-03-23
- Ability to edit/delete comments
- Issue: discard button don't work

## [] - 2021-03-24
- User profile page.
  - Everyone must have an ability to see user page.
  - URL of user page must be named as user (user nickname must be unique)
  - Profile edit page must be standard and have URL ".../profile/"
- User profile edit page.
- Add JWT Auth. API.
- User must have an ability to add avatar, name, surname, email, position
  - This page must have a URL "/profile/edit/"
- User must have an ability to add files to tasks. 
Loading progress bar (JavaScript?)
Add "Comment" button in preview
Notifications
Supertask: add checkbox on the edit page to allow or deny another user to assign tasks to him
Supertask: security questions to recover account password (if answer==true --> set new password)
Supertask: when someone create a new task, in assignee-choicebox by default must be current user
Supertask: class-based views for authorization
  
## [] - 2021-03-25
- Link to profile of commentator and/or who has assigned task
Issue: when edit user name > all tasks and permissions are failed?
  
## [] - 2021-03-29
- Issue: filter doesn't work with assigned tasks
- Task deadline ability
- Issue with "user has no profile" when old user try to log in
add ability to add first/last names
rewrite permissions to "Castom permissions"
  
## [] - 2021-03-31
- Expand API capabilities: profile, user
issue: when task created without deadline - preview show "deadline is missed"
delete docs when user update attachment/avatar
clean code

## [] - 2021-04-01
После того как я создал тест и прогнал их - нашел уязвимость в форме авторизации. Если ввести неправильный пароль
try метод вызовет except и попытается создать profile для указанного юзернейма, хотя он уже существует.

## [v0.6.1] - 2021-04-02
### Added
- Profile birthday validation (can be not equal or greater than today).
- New tests to check if handle with tasks and comments via GET/POST requests is securely and correct.
### Fixed
### Security
- Any user could change task status of any task by id.
- Any user could add comments to disallowed tasks.

## [v0.6.2] - 2021-04-05
### Added
- comment length validation in API (comment text must have at least 1 character)
### Security
- Anyone could edit comments by ID via API PUT (in 'api/permissions.py' added 'CommentRUDPermission')
### Fixed
- TODO: issue: cannot update picture
- TODO: issue: cannot edit profile via API without a picture.
- issue: cannot create a task via API without attachment.
- issue: it is possible to create a task via API without an assignee (every task must have assignee).
- issue: it is possible to create a task via API with 3 characters and less.
- issue: impossible to create comment via POST API method (Does not exist - api/views.py - 98 line)
- issue: it is possible to create a comment via API without "task" and "commentator" (every comment must be 
  assigned to task and must have commentator id)
- issue: CommentPostAPIView does not attach request.user id to comment (fixed on: api/views.py - 100 line)
- issue: 'commentator' and 'task' model fields can be null (fixed on: tasks/models.py - 45, 47 lines)
- issue: added "get_object_or_404" instead of "get" (Does not exist - api/views.py - 98 line)

## [v0.6.3] - 2021-04-06
### Added
- list of comment by the task ID (API)
- comments to classes in api/permissions.py
- clean code and comments to classes in api/views.py
- history feature
### Removed
- unused 'isOwnerOrReader' class from api/serializers.py
### Fixed
- set task model fields 'assignee' and 'user' again to 'True' and added 'attr' 'required' to serializer. 
- renamed api/premissions.py to api/permissions.py
- unused 'CommentCreatePermission' class had no effect and always returned 'True'
- it was impossible to list all comments via API 'comment-list'. Fixed 'CommentCreatePermission' in api/permissions.py

## [v0.6.4] - 2021-04-07
### Added
- new history design and functionality
### Fixed
- deadline don't work. (fixed in tasks/forms.py clean_deadline)
- impossible to delete an assigned task from detail view (added separate html's for a preview and detail view)

## [v0.6.5] - 2021-04-08
### Added
- add a simple chat from Django Channels tutorial
- TODO: see on history page of task what was changed (based on diff-match-patch library)
- new requirements

## [v0.6.6] - 2021-04-09
### Added
- added DevOps mode to the chat (experimental)
### Fixed
- after create a task in history "Edited without changes" instead of "Created" (template bug*)

## [v0.6.7] - 2021-04-12
### Added
- RESTful API for an upcoming chat v2.0
- Edited static files location
- added JS folder to static files and import js in a template with <script src="{% static 'js/file.js %}"></script>

## [v0.6.8] - 2021-04-13
### Added
- TODO: make chat asynchronous
- TODO: chat v2.0
  - TODO: js script for chat 2.0 web app
  - TODO: resolve security problems
  - TODO: write tests to check if all is ok
- TODO: delete a photo from a database when user change it or delete it
- TODO: API authentication
### Fixed
- /chat/api/permissions.py line 58 - django.utils.datastructures.MultiValueDictKeyError: 'sender'
- bugs with chat API views

## [v0.6.9] - 2021-04-14
### Added
- User can create new chat
- Validation, if chat with this name is already exists.
- some visual improvements in HTML
### Fixed
- could not send messages via JavaScript jQuery (Permission Denied 403) - added ajaxSetup to 'chat.js'
- cleaned code

## [v0.6.10] - 2021-04-15
### Added
### Fixed

## [v0.6.11] - 2021-04-16
### Added
### Fixed
- cannot send POST Create Chat without user list (user must have an ability to create blank chat)
- everyone has access to all chats
- if user create chat and try to add new user and then click 'invite' --> redirect him to lobby and say 'chad does not exist'
- when user try to leave the chat - delete all users from the chat

## [v0.7.0] - 2021-04-19
### Added
### Fixed

## [v0.7.1] - 2021-04-20
### Added
### Fixed

## [v0.7.2] - 2021-04-21
### Added
- when user log out - add refresh token from cookies to the blacklist. (lines 101-103 in accounts/views.py)
- when user log out - delete access token from cookies (added 'main.js' script inside base.html)
- focus on input fields (chat, invite users)
- some HTML/CSS improvements
### Fixed
- when user leave the chat, kick all other users. (line 123 in chat/api/views.py)
- user must reload chat page when his access token is expired.
- code in JS was refactored
- JS function loop when user try to delete chat.

## [v0.7.3] - 2021-04-22
### Added
- updated requirements.txt
### Fixed

## [v0.7.4] - 2021-04-23
### Added
- API features to back up user's tasks and comments (currently 2 .csv files in .zip file)
### Fixed
- Added JWT authorization (header with access token) to API tests.

## [v0.7.5] - 2021-04-26
### Added
- If task has attachment - add it to .zip archive.
- Alpha version of backup export.

## [v0.7.6] - 2021-04-27
### Added
- parser for .zip file with .csv files and attachments.
### Fixed

## [v0.7.7] - 2021-04-28
### Added
### Fixed

## [v0.7.8] - 2021-04-29
### Added
- Refactored backup code
### Fixed
- Task backup don't save user id. Created new Serializer 'TaskBackupSerializer' to solve this problem.
- Cannot attach attachment to task when import it.

## [v0.7.9] - 2021-04-30
### Added
- tests for backup
- API for task history
### Fixed

## [v0.7.10] - 2021-05-05
### Added
- if some fields in .csv are missing -> return Bad Request 400 (lines 281-282, 311-312 in tasks/api/views.py)
- check if task id is an integer. (lines 284-285 in tasks/api/views.py)
- try/except for 'obj' and description in TaskHistoryListAPIView
### Fixed
- 'elif' instead of 'if' (tasks/api/views.py line 290)
- if file with 'attachment' field value name not in .zip file --> don't create this file.
- rewrote Create API view. Now user must send task 'pk' via POST, not via GET kwargs.
- change 'commentator_id' and 'task_id' to 'commentator' and 'task' in CommentSerialize

## [v0.7.11] - 2021-05-06
### Added
- chat pagination with button 'see more'
- implemented two new chat mods.
- updated README.md
- user can delete messages
- admin can create public chat
### Fixed
- 

## [v0.7.12] - 2021-05-07
### Added
- default pictures
- when user delete chat, and he is the last member -> destroy chat and redirect to lobby.
### Fixed
- prevented infinite loop in JS (marker added)
- when user log in -> automatically obtain tokens.
- if user is deleted - set standard picture to his message.
- user cannot create chat (Permission Denied 403). (chats/api/permissions.py line 89)

## [v.0.x.x] - 2021-05-12
### Added
- Develop word game.
### Fixed
- When user set wrong filter - show all tasks of another users. (fixed: TaskListView in tasks/views.py).
- Removed some buttons from navbar for not logged-in users.

## [v.0.x.x] - 2021-05-13
### Added
- Word game improvements.
- Pagination on home page.
- User get 404 when try to go to invite url of public chat (e.g. /chat/40/invite/ where chat with id 40 is public).
- When user updated attachment or picture or delete task - delete file from the server.
### Fixed
- Deleted "invite" button for public chats.
- Profile datepicker. (added {{ profile_form.media }} in template).

## [v.0.x.x] - 2021-05-14
### Added
- Logging for backups.
- Timestamp to tasks.
### Fixed

## [v.0.x.x] - 2021-05-17
### Added
- Error messages in log-in page.
- Error messages in game page.
- When user leave the game - another user get alert.
### Fixed
- When user in /task/create/ etc. -> clean cache -> reload page -> 404 Error. Added "login_url" to all 'task app' views.
- When user has no picture - cannot load chat messages. (fixed in chat/api/serializers.py MessageSerializer)

## [v.0.x.x] - 2021-05-18
### Added
- Chat API tests
### Fixed
- KeyError when user try to create chat via API without 'is_public' field.

## [v.0.x.x] - 2021-05-19
### Added
- check if backup file not bigger than 100mb.
### Fixed
- user without picture cannot add this because of error with default picture (cannot find the path).

## [v.0.x.x] - 2021-05-20
### Added
### Fixed
- anyone can see task history. (fixed on tasks/views.py with lines 225-235)
- anyone can see message list of every private chat. (has_object_permission with ListAPIView doesn't work.)
- anyone can see member list of every private chat. (has_object_permission with ListAPIView doesn't work.)
- anyone can search members.
- error when user try to create message without mode. (serializer)

## [v.0.x.x] - 2021-05-24
### Added
- Now user can't backup tasks with another owner id.
### Fixed

FIXME: user can log in via JWT without login to the site.
TODO: add notifications.
TODO: user can delete all tasks.
TODO: user can delete account.
TODO: limit number of tasks/comments, that user can create in a day.