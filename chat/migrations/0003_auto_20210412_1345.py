# Generated by Django 3.1.7 on 2021-04-12 10:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20210327_1913'),
        ('chat', '0002_groupmodel_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='messagemodel',
            name='sender',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='sender', to='accounts.profile'),
        ),
    ]
