from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APITestCase

from chat.models import MessageModel, GroupModel
from accounts.models import Profile

DEFAULT_PIC_URL = '/media/default-user-image.png'
DEFAULT_NO_PIC_URL = '/media/undefined-user-image.png'


class ChatAPITestCase(APITestCase):

    def setUp(self):
        """Create user, obtain token and create messages."""
        # Create user
        self.username = 'test'
        self.email = 'test@mail.com'
        self.password = 'test'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)

        # Create another user
        self.a_username = 'foo'
        self.a_email = 'foo@foo.com'
        self.a_password = 'foo'
        self.a_user = User.objects.create_user(username=self.a_username, email=self.a_email, password=self.a_password)

        # create chat and message.
        self.chat = GroupModel.objects.create(name='test', creator=self.user)
        self.chat.users.add(self.user)
        self.text = 'test'
        self.msg = MessageModel.objects.create(message=self.text, sender=self.user, recipient=self.chat)

        self.assertEqual(self.chat.is_public, False)

        # check
        profile = Profile.objects.last()
        self.assertEqual(profile.picture.url, DEFAULT_PIC_URL)

    def _obtain_token(self, _username, _password):
        url = reverse('token-obtain-pair')
        data = {'username': _username, 'password': _password}
        response = self.client.post(url, data, format='json')
        token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)

    # --------- GET message API ---------

    def test_get_message_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        msg_id = self.msg.id
        url = reverse('message-rud-api', kwargs={'pk': msg_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['message'], self.text)
        self.assertEqual(response.data['sender'], self.username)
        self.assertEqual(response.data['recipient'], self.chat.id)
        self.assertEqual(response.data['picture'], DEFAULT_PIC_URL)
    
    def test_get_message_disallowed_API(self):
        """"""
        # log in with another user.
        self.client.login(username=self.a_username, password=self.a_password)
        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_password)

        msg_id = self.msg.id
        url = reverse('message-rud-api', kwargs={'pk': msg_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.assertRaises(KeyError, lambda: response.data['message'])
        self.assertRaises(KeyError, lambda: response.data['sender'])
        self.assertRaises(KeyError, lambda: response.data['recipient'])
        self.assertRaises(KeyError, lambda: response.data['picture'])

    def test_get_message_bad_message_id_API(self):
        """"""
        # log in with another user.
        self.client.login(username=self.a_username, password=self.a_password)

        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_password)

        msg_id = 404  # bad message ID.
        url = reverse('message-rud-api', kwargs={'pk': msg_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    # --------- GET chat API ---------

    def test_get_chat_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)
        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = self.chat.id
        url = reverse('chat-ru-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], self.chat.name)
        self.assertEqual(response.data['creator'], self.chat.creator.username)
        self.assertEqual(response.data['is_public'], False)
        self.assertEqual(len(response.data['users']), 1)

    def test_get_chat_disallowed_API(self):
        """"""
        # log in.
        self.client.login(username=self.a_username, password=self.a_password)
        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_password)

        chat_id = self.chat.id
        url = reverse('chat-ru-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.assertRaises(KeyError, lambda: response.data['message'])
        self.assertRaises(KeyError, lambda: response.data['sender'])
        self.assertRaises(KeyError, lambda: response.data['recipient'])
        self.assertRaises(KeyError, lambda: response.data['picture'])

    def test_get_chat_bad_chat_id_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = 404  # bad chat id
        url = reverse('chat-ru-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    # --------- chat list API ---------

    def test_get_chat_list_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)
        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        url = reverse('chat-list-api')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['name'], self.chat.name)
        self.assertEqual(response.data[0]['creator'], self.chat.creator.username)
        self.assertEqual(response.data[0]['is_public'], False)
        self.assertEqual(len(response.data[0]['users']), 1)

    def test_get_chat_list_disallowed_API(self):
        """"""
        # log in with another user.
        self.client.login(username=self.a_username, password=self.a_password)
        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_password)

        url = reverse('chat-list-api')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    # --------- create chat API ---------

    def test_create_chat_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)
        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        url = reverse('chat-create-api')
        chat_name = 'foo'
        data = {'name': chat_name, 'is_public': 'False'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(GroupModel.objects.count(), 2)
        self.assertEqual(response.data['name'], chat_name)
        self.assertEqual(response.data['creator'], self.chat.creator.username)
        self.assertEqual(response.data['is_public'], False)
        self.assertEqual(len(response.data['users']), 1)

    def test_create_chat_disallowed_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)
        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        url = reverse('chat-create-api')
        chat_name = 'foo'
        data = {'name': chat_name, 'is_public': 'True'}  # is_public = True is disallowed (only for superusers)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(GroupModel.objects.count(), 1)

    # --------- message list API ---------

    def test_chat_message_list_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)
        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = self.chat.id
        url = reverse('chat-messages-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data['results']), 1)

    def test_chat_message_list_disallowed_API(self):
        """"""
        # log in.
        self.client.login(username=self.a_username, password=self.a_password)

        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_password)

        chat_id = self.chat.id
        url = reverse('chat-messages-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.assertRaises(KeyError, lambda: response.data['results'])

    def test_chat_message_list_bad_chat_id_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = 404  # bad chat id.
        url = reverse('chat-messages-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.assertRaises(KeyError, lambda: response.data['results'])

    # --------- chat members API ---------

    def test_chat_members_list_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = self.chat.id
        url = reverse('chat-members-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['user'], self.username)

    def test_chat_members_list_disallowed_API(self):
        """"""
        # log in.
        self.client.login(username=self.a_username, password=self.a_password)

        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_password)

        chat_id = self.chat.id
        url = reverse('chat-members-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        # self.assertRaises(KeyError, lambda: response.data['results'])

    def test_chat_members_list_bad_chat_id_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = 404  # bad chat id.
        url = reverse('chat-members-api', kwargs={'pk': chat_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    # --------- search members API ---------

    def test_chat_search_members_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = self.chat.id
        query = 'f'  # first two characters of username.
        url = reverse('chat-users-search-api', kwargs={'pk': chat_id}) + "?q=" + query
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['username'], self.a_username, msg='Filter is broken. Check your code.')

    def test_chat_search_members_bad_q_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = self.chat.id
        query = 'error'  # bad query parameter.
        url = reverse('chat-users-search-api', kwargs={'pk': chat_id}) + "?q=" + query
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    def test_chat_search_members_disallowed_API(self):
        """"""
        # log in.
        self.client.login(username=self.a_username, password=self.a_password)

        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_password)

        chat_id = self.chat.id
        query = 'f'  # first two characters of username.
        url = reverse('chat-users-search-api', kwargs={'pk': chat_id}) + "?q=" + query
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_chat_search_members_bad_chat_id_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = 404  # bad chat id.
        query = 'f'  # first two characters of username.
        url = reverse('chat-users-search-api', kwargs={'pk': chat_id}) + "?q=" + query
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    # --------- chat delete API ---------

    def test_chat_delete_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = self.chat.id
        url = reverse('chat-delete-api', kwargs={'pk': chat_id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)

    def test_chat_delete_disallowed_API(self):
        """"""
        # log in.
        self.client.login(username=self.a_username, password=self.a_password)

        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_username)

        chat_id = self.chat.id
        url = reverse('chat-delete-api', kwargs={'pk': chat_id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 403)

    def test_chat_delete_bad_chat_id_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = 404  # bad chat id.
        url = reverse('chat-delete-api', kwargs={'pk': chat_id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 404)

    # --------- chat leave API ---------

    def test_chat_leave_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = self.chat.id
        url = reverse('chat-leave-api', kwargs={'pk': chat_id})
        data = {'self': 'True'}
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, 202)
        self.assertEqual(len(response.data), 1)

    def test_chat_leave_disallowed_API(self):
        """"""
        # log in.
        self.client.login(username=self.a_username, password=self.a_password)

        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_username)

        chat_id = self.chat.id
        url = reverse('chat-leave-api', kwargs={'pk': chat_id})
        data = {'self': 'True'}
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(len(response.data), 1)
        self.assertIn('You are not in this chat or chat is public', response.data['err'])

    def test_chat_leave_bad_chat_id_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = 404  # bad chat id.
        url = reverse('chat-leave-api', kwargs={'pk': chat_id})
        data = {'self': 'True'}
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(len(response.data), 1)

    # --------- message create API ---------

    def test_chat_create_message_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = self.chat.id
        url = reverse('message-create-api')
        data = {'message': 'test', 'recipient': chat_id}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(len(response.data), 6)

    def test_chat_create_message_disallowed_API(self):
        """"""
        # log in.
        self.client.login(username=self.a_username, password=self.a_password)

        # obtain JWT token.
        self._obtain_token(self.a_username, self.a_username)

        chat_id = self.chat.id
        url = reverse('message-create-api')
        data = {'message': 'test', 'recipient': chat_id}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(len(response.data), 1)

    def test_chat_create_message_bad_chat_id_API(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        self._obtain_token(self.username, self.password)

        chat_id = 404  # bad chat id.
        url = reverse('message-create-api')
        data = {'message': 'test', 'recipient': chat_id}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(len(response.data), 1)
