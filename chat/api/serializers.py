from django.contrib.auth.models import User
from rest_framework import serializers

from chat.models import MessageModel, GroupModel
from accounts.models import Profile

def_del_pic = '/media/deleted-user-image.png'
def_und_pic = '/media/undefined-user-image.png'


class MessageSerializer(serializers.ModelSerializer):
    sender = serializers.ReadOnlyField(source='sender.username')
    picture = serializers.SerializerMethodField()

    def get_picture(self, obj):
        """If user has no profile or deleted - set default picture to his messages."""
        # (DONE) FIXME: if user was deleted: Profile matching query does not exist.
        try:
            profile = Profile.objects.get(user_id=obj.sender_id)
            try:
                pic = profile.picture.url
                return pic
            except Exception as exc:
                return def_und_pic
        except Profile.DoesNotExist:
            return def_del_pic

    class Meta:
        model = MessageModel
        fields = [
            'pk',
            'message',
            'sender',
            'recipient',
            'timestamp',
            'picture',
        ]
        read_only_fields = ['sender']

    def validate_message(self, value):
        try:
            mode = self.initial_data['mode']
        except KeyError:
            return value
        if mode == 'devops':
            mod_value = value
            for symbol in [')', '(', '.', ',', ';', ':', '>', '<', '/', ']', '^', '_', '-', '?', '!', '*']:
                mod_value = mod_value.replace(symbol, "")
            return mod_value.upper() + " БЛЯТЬ"
        return value


class GroupSerializer(serializers.ModelSerializer):
    creator = serializers.ReadOnlyField(source='creator.username')

    class Meta:
        model = GroupModel
        fields = [
            'name',
            'creator',
            'users',
            'is_public',
        ]
        read_only_fields = ['creator']

    def validate_name(self, value):
        """
        Check that the chat with this name is not exist.
        """
        qs = GroupModel.objects.filter(name__iexact=value)
        if qs.exists():
            raise serializers.ValidationError("Chat with this name is already exists.")
        return value


class UserChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
        ]
        read_only_fields = ['username', 'email']


class MemberSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Profile
        fields = [
            'user',
            'picture',
        ]
        read_only_fields = ['user']
