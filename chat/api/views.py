from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework import filters, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveUpdateAPIView,
    DestroyAPIView,
    UpdateAPIView, RetrieveUpdateDestroyAPIView
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from .serializers import MessageSerializer, GroupSerializer, MemberSerializer, UserChatSerializer
from chat.models import MessageModel, GroupModel
from accounts.models import Profile
from .permissions import (
    MessagePermissions,
    GroupPermissions,
    MessageCreatePermissions,
    ChatDeletePermissions,
    ChatLeavePermissions,
    GroupCreatePublicPermissions,
)


# TODO: add swagger_auto_schema


class SmallResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 100


class MessagesListAPIView(ListAPIView):
    """List of messages in a specific group."""
    serializer_class = MessageSerializer
    permission_classes = [IsAuthenticated, MessagePermissions]
    pagination_class = SmallResultsSetPagination

    def get_queryset(self):
        group_id = self.kwargs['pk']
        queryset = MessageModel.objects.all().filter(recipient_id=group_id)
        return queryset.order_by('-timestamp')  # queryset --> queryset.order_by('-timestamp')


class MessageRUDAPIView(RetrieveUpdateDestroyAPIView):
    """Particular message by id."""
    serializer_class = MessageSerializer
    permission_classes = [IsAuthenticated, MessagePermissions]
    lookup_field = 'pk'

    def get_queryset(self):
        return MessageModel.objects.all()


class MessageCreateAPIView(CreateAPIView):
    """Create new message."""
    serializer_class = MessageSerializer
    permission_classes = [IsAuthenticated, MessageCreatePermissions]

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(sender=self.request.user)


class GroupListAPIView(ListAPIView):
    """List of available groups."""
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """get queryset with public groups and private groups (if user in group)"""
        queryset = GroupModel.objects.filter(Q(users__username=self.request.user.username) | Q(is_public=1))
        return queryset

    # def check_permissions(self, request):
    #     return super(GroupListAPIView, self).check_permissions(request)


class MembersListAPIView(ListAPIView):
    """List of members in a particular group."""
    serializer_class = MemberSerializer
    permission_classes = [IsAuthenticated, GroupPermissions]

    def get_queryset(self):
        """Returns queryset of chat members."""
        chat_id = self.kwargs['pk']
        chat = GroupModel.objects.get(pk=chat_id)
        queryset = Profile.objects.filter(user_id__in=chat.users.values_list('pk'))
        return queryset


class SearchUsersListAPIView(ListAPIView):
    """List of filtered users."""
    serializer_class = UserChatSerializer
    permission_classes = [IsAuthenticated, GroupPermissions]
    filter_backends = [filters.SearchFilter]

    def get_queryset(self):
        """Returns queryset with users, filtered by request string 'q'."""
        q = self.request.query_params['q']
        chat_id = self.kwargs['pk']
        if not q.strip():
            queryset = self.queryset
        else:
            group = GroupModel.objects.get(pk=chat_id)
            queryset = User.objects.filter(username__icontains=q).exclude(pk__in=list(group.users.values_list('pk', flat=True)))
        return queryset


class ChatDeleteAPIView(DestroyAPIView):
    """Delete chat if user is chat owner."""
    serializer_class = UserChatSerializer
    permission_classes = [ChatDeletePermissions]
    lookup_field = 'pk'

    def get_queryset(self):
        return GroupModel.objects.all()


class GroupLeaveUpdateAPIView(UpdateAPIView):
    """
    Delete user from user list of specific chat, but only if user is in chat and is not owner of PUBLIC chat.
    If user, that has left the chat, was last member of PRIVATE chat --> delete chat.
    """
    serializer_class = GroupSerializer
    permission_classes = [ChatLeavePermissions]

    def get_queryset(self):
        return GroupModel.objects.all()

    def update(self, request, *args, **kwargs):
        # list with chat members pks
        cur_users_id = list(self.get_object().users.all().values_list('pk', flat=True))
        # try to delete user from chat list.
        try:
            cur_users_id.remove(request.user.id)  # FIXED
        except Exception as exc:
            return Response({"err": f"You are not in this chat or chat is public - {exc}"},
                            status=status.HTTP_400_BAD_REQUEST)
        if 'self' not in request.data:
            raise PermissionDenied()
        # if no users in chat more --> delete chat.
        if not cur_users_id:
            chat = GroupModel.objects.get(pk=kwargs['pk'])
            if not chat.is_public:
                chat.delete()
                return Response({"err": f"Chat was deleted"},
                                status=status.HTTP_202_ACCEPTED)
            else:
                return Response({"err": f"You are admin in public chat"},
                                status=status.HTTP_400_BAD_REQUEST)
        # update request user list.
        else:
            request.data.update({'users': cur_users_id})
        del request.data['self']
        return super(GroupLeaveUpdateAPIView, self).update(request, *args, **kwargs)


class GroupRuAPIView(RetrieveUpdateAPIView):
    """Get information about specific chat room."""
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated, GroupPermissions]

    def get_queryset(self):
        return GroupModel.objects.all()

    def update(self, request, *args, **kwargs):
        users_id = list(User.objects.filter(username__in=list(request.data.values())[0]).values_list('pk', flat=True))
        cur_users_id = list(self.get_object().users.all().values_list('pk', flat=True))
        request.data.update({'users': cur_users_id + users_id})
        return super(GroupRuAPIView, self).update(request, *args, **kwargs)


class GroupCreateAPIView(CreateAPIView):
    """Create new group."""
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated, GroupCreatePublicPermissions]

    def perform_create(self, serializer):
        serializer.validated_data['users'] = []
        serializer.validated_data['users'].append(self.request.user.id)
        serializer.save(creator=self.request.user)

    def post(self, request, *args, **kwargs):
        must_fields = ['name', 'is_public']
        if not all(elem in request.data for elem in must_fields):
            diff = tuple(set(must_fields) - set(request.data))
            return Response({"err": f"This fields are missing - {diff}."}, status=status.HTTP_400_BAD_REQUEST)
        return self.create(request, *args, **kwargs)
