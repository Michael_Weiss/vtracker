from rest_framework import permissions

from chat.models import GroupModel, MessageModel
from accounts.models import Profile


# from django.db.models import Q


class GroupPermissions(permissions.BasePermission):
    """"""

    def has_permission(self, request, view):
        try:
            group = GroupModel.objects.get(pk=view.kwargs['pk'])
        except GroupModel.DoesNotExist:
            return False
        roommates = group.users.all()

        # if user in the message group --> has perm to see it
        try:
            member = roommates.filter(username=request.user).first()
        except Profile.DoesNotExist:
            member = None

        if request.method in permissions.SAFE_METHODS and member is not None:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        group = GroupModel.objects.get(pk=obj.pk)
        roommates = group.users.all()

        # if user in the message group --> has perm to see it
        try:
            member = roommates.filter(username=request.user).first()
        except Profile.DoesNotExist:
            member = None

        if request.method in permissions.SAFE_METHODS and member is not None:
            return True
        return obj.creator == request.user or request.user in roommates


class MessagePermissions(permissions.BasePermission):
    """"""

    def has_permission(self, request, view):
        # view.kwargs['pk']
        try:
            group = GroupModel.objects.get(pk=view.kwargs['pk'])
        except GroupModel.DoesNotExist:
            return False  # FIXME: must be 404, not 403
        roommates = group.users.all()

        # if user in the message group --> has perm to see it
        try:
            member = roommates.filter(username=request.user.username).first()  # FIXME: add creator to allowed users
        except Profile.DoesNotExist:
            member = None

        if request.method in permissions.SAFE_METHODS and (member is not None or group.is_public):
            return True
        return False

    def has_object_permission(self, request, view, obj):
        message = MessageModel.objects.get(pk=obj.pk)
        group = GroupModel.objects.get(pk=message.recipient_id)
        roommates = group.users.all()

        # if user in the message group --> has perm to see it
        try:
            member = roommates.filter(username=request.user.username).first()  # FIXME: add creator to allowed users
        except Profile.DoesNotExist:
            member = None

        if request.method in permissions.SAFE_METHODS and (member is not None or group.is_public):
            return True
        return obj.sender == request.user


class MessageCreatePermissions(permissions.BasePermission):
    """"""

    def has_permission(self, request, view):
        group_id = request.data['recipient']
        try:
            group = GroupModel.objects.get(pk=group_id)
        except GroupModel.DoesNotExist:
            return True
        roommates = group.users.all()
        # if user in the message group --> has perm to see it
        try:
            member = roommates.filter(username=request.user).first()  # FIXME: add creator to allowed users
        except Profile.DoesNotExist:
            member = None

        return member is not None or group.is_public


class ChatDeletePermissions(permissions.BasePermission):
    """"""

    def has_object_permission(self, request, view, obj):
        if obj.creator == request.user:
            return True
        else:
            return False


class ChatLeavePermissions(permissions.BasePermission):
    """"""

    def has_permission(self, request, view):
        return True


class GroupCreatePublicPermissions(permissions.BasePermission):
    """"""

    def has_permission(self, request, view):
        """"""
        try:
            is_pub = request.data['is_public']
        except Exception:
            return True
        if is_pub and request.user.is_staff:  # is_public is True and request.user is_staff is True
            return True
        elif is_pub.lower() == 'false':  # is_public is False
            return True
        else:
            return False
