from django.urls import path

from .views import (
    GroupCreateAPIView,
    GroupListAPIView,
    GroupRuAPIView,
    MembersListAPIView,
    ChatDeleteAPIView,
    MessagesListAPIView,
    MessageRUDAPIView,
    MessageCreateAPIView,
    SearchUsersListAPIView,
    GroupLeaveUpdateAPIView,
)


urlpatterns = [
    path('chat/', GroupCreateAPIView.as_view(), name='chat-create-api'),  # tested (200, 403)
    path('chats/', GroupListAPIView.as_view(), name='chat-list-api'),  # tested (200)
    path('chat/<int:pk>/', GroupRuAPIView.as_view(), name='chat-ru-api'),  # tested (200, 403, 404)
    path('chat/<int:pk>/users/', MembersListAPIView.as_view(), name='chat-members-api'),  # tested (200, 403, 404)
    path('chat/<int:pk>/messages/', MessagesListAPIView.as_view(), name='chat-messages-api'),  # tested (200, 403, 404)
    path('chat/<int:pk>/search/', SearchUsersListAPIView.as_view(), name='chat-users-search-api'),  # tested (200...)
    path('chat/<int:pk>/delete/', ChatDeleteAPIView.as_view(), name='chat-delete-api'),  # tested (200, 403, 404)
    path('chat/<int:pk>/leave/', GroupLeaveUpdateAPIView.as_view(), name='chat-leave-api'),  # tested (200, 403, 400)
    path('message/', MessageCreateAPIView.as_view(), name='message-create-api'),  # ?tested ()
    path('message/<int:pk>/', MessageRUDAPIView.as_view(), name='message-rud-api'),  # tested (200, 403, 404)
]
