from django.http import Http404
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from .models import GroupModel

# TODO: 2021-04-12
# - Ability to add users to 'GroupModel' model when you are creator of this 'GroupModel' object.
# - Ability to kick users form the 'GroupModel' model when you are creator of this 'GroupModel' object.
# - Send previous massages related to 'GroupModel' (e.g. 8 last messages) via JSON (how? don't know!)
# - Send all users (with pictures) of this chat to user's HTML JavaScript and show who is in chat and who is offline
# - Add notifications when user has new messages
# - Ability to mute chats

# TODO: 2021-04-15
# - rewrite to generic views


def chat_select(request):
    return render(request, 'chat_select.html')


def chat_enter(request, room_name):
    chat_exists = GroupModel.objects.filter(name=room_name).exists()
    if chat_exists:
        chat = GroupModel.objects.get(name=room_name)
        if request.user in chat.users.all() or chat.is_public:  # 06.05.2021 added: chat.is_public
            return render(request, 'chat_room.html', {
                'room_name': room_name,
                'room_id': chat.pk,
                'is_public': chat.is_public,
                'creator': chat.creator,
            })
        else:
            raise PermissionDenied
    elif not chat_exists:
        return render(request, 'chat_select.html', {
            'room_name': room_name,
            'error': 'This chat is not exists.'
        })
    else:
        return Http404


def chat_create(request, room_name):
    # TODO: try to add validation by the chat name. If name is already exists --> validation error.
    chat_room = GroupModel.objects.get_or_create(name=room_name)
    if chat_room[1] is True:
        chat_room[0].creator_id = request.user.id
        chat_room[0].save()
        return render(request, 'chat_room.html', {
            'room_name': chat_room[0].name
        })
    elif chat_room[1] is False:
        return render(request, 'chat_select.html', {
            'room_name': chat_room[0].name,
            'error': 'This chat is already exists.'
        })
    else:
        return Http404


def login(request):
    return render(request, 'login_chat.html', {})


def chat_invite(request, room_id):
    try:
        chat_room = GroupModel.objects.get(pk=room_id)
    except GroupModel.DoesNotExist:
        raise Http404
    if not chat_room.is_public:
        return render(request, 'chat_invite.html', {'room_name': chat_room.name, 'room_id': room_id})
    else:
        raise Http404
