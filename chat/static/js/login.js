export function ajaxSetup(){
    return new Promise((resolve, reject) => {
        let tokenIsValid = checkOrRefreshToken();
        tokenIsValid.then(function(result) {
            // if token is OK (true) --> add token to the header.
            console.log(result + ' - result of "checkOrRefreshToken" function.')
            if (result === true){
                let token = getCookie('access-token');
                console.log('Token from getCookie() - ' + token)
                $.ajaxSetup({
                    headers: {
                        "Authorization": "Bearer " + token
                    }
                });
                // return true;
                resolve();
            } else {
                // return false;
                reject();
            }
        })
    })
}

function checkOrRefreshToken(){
    // Check that access token is not expired:
    // - If token is not expired --> __return true__
    // - If token is expired --> send refresh token and retrieve new access token:
    // - - If access token is received --> __return true__
    // - - If refresh token is expired --> redirect to login page.
    let resp = checkToken().then(async function(mess) {
        // if access token is ok:
        console.log(mess + ' - message from checkToken function.')
        if (mess === 'invalid') {
            console.log('Trying to refresh access token...')
            let result = refreshToken().then(function () {
                // if refresh token is ok:
                console.log('Access token was successfully refreshed!')
                return true;
            }).catch(function () {
                // if refresh token is expired:
                console.log('Refresh token is expired. Please log in again.')
                window.location.replace(`/chat/login/`);
                return false;
            })
            console.log(await result + ' - result of refreshToken function.')
            return result
        } else {
            console.log('Access token is valid.')
            return true;
        }
    })
    return resp;
}

function getCookie(name) {
    let v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}

function setCookie(name, value, days) {
    var d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString() + ";SameSite=Strict";
}

function checkToken(){
    return new Promise(function(resolve, reject) {
        let token = getCookie('access-token');
        let data = {'token': token};
        let request = $.ajax({
            url: `/api/token/verify/`,
            data: JSON.stringify(data),
            type: 'POST',
            contentType: 'application/json',
            processData: false,
            dataType: 'json'
        });

        request.fail(function () {
            resolve('invalid');
        });
        request.done(function () {
            resolve('valid');
        });
    });
}

function refreshToken(){
    return new Promise(function(resolve, reject) {
        let token = getCookie('refresh-token');
        let data = {'refresh': token};
        let request = $.ajax({
            url: `/api/token/refresh/`,
            data: JSON.stringify(data),
            type: 'POST',
            contentType: 'application/json',
            processData: false,
            dataType: 'json'
        });

        request.fail(function () {
            reject();
        });
        request.done(function () {
            setCookie('access-token', arguments[0].access, 2);
            resolve();
        });
    });
}

function obtainToken(){
    return new Promise(function(resolve, reject) {
        let usr = document.getElementById("login-input").value;
        let pwd = document.getElementById("password-input").value;
        let data = {'username': usr, 'password': pwd};
        let request = $.ajax({
            url: `/api/token/`,
            data: JSON.stringify(data),
            type: 'POST',
            contentType: 'application/json',
            processData: false,
            dataType: 'json'
        });
        request.done(function (data) {
            setCookie('access-token', arguments[0].access, 2);
            setCookie('refresh-token', arguments[0].refresh, 2);
            resolve(data);
        });
        request.fail(function (err) {
            reject(err.responseJSON.detail);
        });
    });
}

$( document ).ready(function() {
    $('#needs-validation').submit(function (event) {
        event.preventDefault();
        if ($('#needs-validation')[0].checkValidity() === false) {
            event.stopPropagation();
            $('#inv-name').empty().append('Type something.');
        } else {
            event.stopPropagation();
            obtainToken().then(function() {
                // Run this when your request was successful
                window.location.replace(`/chat/`);
            }).catch(function(err) {
                // Run this when promise was rejected via reject()
                $('#inv-name').empty().append(err);
                $('#needs-validation')[0].reset();
            })
        }
        $('#needs-validation').addClass('was-validated');
    });
});