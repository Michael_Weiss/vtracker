function setCookie(name, value, days) {
    var d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString() + ";SameSite=Strict";
}

function obtainToken(){
    return new Promise(function(resolve, reject) {
        let usr = document.getElementById("login-input").value;
        let pwd = document.getElementById("password-input").value;
        let data = {'username': usr, 'password': pwd};
        let request = $.ajax({
            url: `/api/token/`,
            data: JSON.stringify(data),
            type: 'POST',
            contentType: 'application/json',
            processData: false,
            dataType: 'json'
        });
        request.done(function (data) {
            setCookie('access-token', arguments[0].access, 2);
            setCookie('refresh-token', arguments[0].refresh, 2);
            resolve(data);
        });
        request.fail(function (err) {
            reject(err.responseJSON.detail);
        });
    });
}

$( document ).ready(function() {
    $('#needs-validation').submit(function (event) {
        event.preventDefault();
        let elem = document.getElementById("err-msg");
        if (elem !== null){
            elem.parentNode.removeChild(elem);
        }
        if ($('#needs-validation')[0].checkValidity() === false) {
            event.stopPropagation();
            $('#inv-name').empty().append('Type something.');
        } else {
            event.stopPropagation();
            obtainToken().then(function() {
                // Run this when your request was successful
                document.getElementById("needs-validation").submit();
            }).catch(function(err) {
                // Run this when promise was rejected via reject()
                $('#needs-validation')[0].reset();
                $('#inv-name').empty();
                console.log('Cannot obtain token: ' + err);
                $(`<p id="err-msg" style="color: red">${err}</p>`).prependTo(".was-validated");
            })
        }
        $('#needs-validation').addClass('was-validated');
    });
});