function setCookie(name, value, days) {
    var d = new Date;
    d.setTime(d.getTime() + 24*60*60*1000*days);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}

function deleteCookie(name) {
    setCookie(name, '', -1);
}
let logoutButton = document.getElementById('logout-button')
if (logoutButton !== null){
    logoutButton.onclick = function() {
        deleteCookie('access-token')
    };
}