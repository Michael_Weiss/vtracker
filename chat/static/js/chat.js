import {ajaxSetup} from './login.js';

// What must this script do ?
// - retrieve chat users
// - TODO: update users (if someone was deleted - show that)
// - retrieve last n-number of messages
// - send messages
// - handle .onmessage and get message via API by retrieved ID
// - leave/delete chat functionality
// --------------------------------------------------------
// ref: https://github.com/narrowfail/django-channels-chat/
// ref: https://github.com/zhukov/webogram/

// TODO: If user has no pictures --> standard picture.

let chatButton = $('#chat-btn');
let chatInput = $('#chat-input');
let messageList = $('#messages');
let inviteButton = $('#chat-invite-button');
let deleteButton = $('#delete-button');
let leaveButton = $('#leave-button');
const csrftoken = Cookies.get('csrftoken');

$.ajaxSetup({
    headers: {
        'X-CSRFToken': csrftoken
    }
});

function updateUserList(chat_id){
    $.getJSON(`/api/chat/${chat_id}/users/`, function (data) {
        for (let i = 0; i < data.length; i++) {
            const userItem = `
                    <a class="list-group-item user" target="_blank" href="/accounts/profile/${data[i]['user']}/">
                        <div class="profile-image float-md-right">
                            <img src="${data[i].picture}" id="pic-${data[i]['user']}" class="rounded-circle" style="width:70px;height:auto;">    
                        </div>
                        ${data[i]['user']}
                    </a>`;
            $(userItem).appendTo('#user-list');
        }
    });
}

function drawMessage(message, append=false){
    const date = new Date(message.timestamp);
    const messageItem = `
            <li id='message-${message.pk}' class="list-group-item">
                <div class="row">
                    <div class="col-lg-1 col-md-2 col-3">
                        <div class="profile-image float-md-right">
                            <img src="${message.picture}" class="rounded-circle" style="width:70px;height:auto;">    
                        </div>
                    </div>
                    <div class="col-lg col-md col">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="m-t-0 m-b-0">${message.sender}</h5>
                            <small>${date.toLocaleString("en-US")}</small>
                        </div>
                        <p class="mb-1">${message.message}</p>
                        <button id='msg-${message.pk}' type="button" class="close" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </li>`;
    if (append === true){
        $(messageItem).appendTo('#messages');
    } else {
        $(messageItem).prependTo('#messages');
    }
    // add onclick listener to each "delete" icon.
    let x = document.getElementById(`msg-${message.pk}`);
    x.onclick=function(){
        deleteMessage(message.pk);
    }
}

function nextConversation(chat_id, link){
    // split 'next' ling and get page number for 'next' messages.
    let arr = link.split('/')  // link.next.split --> link.split
    let p_num = arr[arr.length - 1]
    // get next messages via API and draw this.
    $.getJSON(`/api/chat/${chat_id}/messages/${p_num}`, function (data) {
        let count = data.results.length - 1;
        for (let i = 0; i <= count; i++) {
            drawMessage(data.results[i]);
        }
        // if GET response has more pages --> add 'load' button to the end of chat list.
        if (data.next != null){
            // add 'load' button to HTML.
            const messageItem = `
                    <li id="next" class="list-group-item" style="display:inline-block; position: relative; text-align: center;">
                        <a href="#" style="color: #1b6d85">Load more...</a>
                    </li>`;
            $(messageItem).prependTo('#messages');
            // add event listener to 'load' button.
            let x = document.getElementById("next");
            x.onclick=function(){
                x.remove();
                nextConversation(chat_id, data.next);  // this.href
            }
        }
    });
}

function getConversation(chat_id){
    $.getJSON(`/api/chat/${chat_id}/messages/`, function (data) {
        messageList.children('.message').remove();
        let count = data.results.length - 1;
        // iterate all messages from GET response.
        for (let i = 0; i <= count; i++) {
            drawMessage(data.results[i]);
        }
        // if GET response has more pages --> add 'load' button to the end of chat list.
        if (data.next != null){
            // add 'load' button to HTML.
            const messageItem = `
                    <li id="next" class="list-group-item" style="display:inline-block; position: relative; text-align: center;">
                        <a href="#">Load more...</a>
                    </li>`;
            $(messageItem).prependTo('#messages');
            // add event listener to 'load' button.
            let x = document.getElementById("next");
            x.onclick=function(){
                x.remove();
                nextConversation(chat_id, data.next);  // this.href
            }
        }
        // scroll message list when new message are drawn.
        messageList.animate({scrollTop: messageList.prop('scrollHeight')});
    });
}

function getMessageByID(message, attempt=true){
    let id = JSON.parse(message).message
    // get message by id via API and draw it.
    $.getJSON(`/api/message/${id}/`, function (data) {
        if (data.recipient === currentChat ||
            (data.recipient === currentChat && data.sender === currentUser)) {
            drawMessage(data, true);
        }
        messageList.animate({scrollTop: messageList.prop('scrollHeight')});
    // if failed - try to draw message again one time.
    }).fail(function () {
        if(attempt===true){
            console.log(arguments[0].responseJSON["detail"]);
            ajaxSetup().then(function (){
                console.log('Trying to get message again...')
                getMessageByID(message, false);
            }).catch(function (){
                console.log('ajax Setup failed.')
            })
        }
    });
}

function sendMessage(chat, message, attempt=true){
    let mode;
    if (document.getElementById('mode').checked){
        mode = "devops";
    } else {
        mode = 'normal'
    }
    $.post(`/api/message/`, {
        recipient: chat,
        message: message,
        mode: mode,
    }).fail(function () {
        if (attempt===true){
            console.log(arguments[0].responseJSON["detail"]);
            ajaxSetup().then(function (){
                console.log('Trying to send message again...')
                sendMessage(chat, message, false);
            }).catch(function (){
                console.log('ajax Setup failed.')
            })
        }
    });
}

function discardMessage(msgId){
    // delete message object .
    let msg = document.getElementById(`message-${msgId}`)
    msg.remove()
}

function deleteMessage(msgId, attempt=true){
    // send 'DELETE' request with message ID via API to the server. If successfully deleted --> status code 204.
    $.ajax({
        url: `/api/message/${msgId}/`,   // search URL (API)
        type: "DELETE",       // Querying means getting in HTTP terms
        success: function (){
            console.log("Message was successfully deleted.");
        }, // Handle success scenario here.
        error: function () {
            console.log(arguments[0].responseJSON["detail"]);
            if (arguments[0].status === 403){
                alert('You do not have permission to perform this action.')
            } else if (attempt===true) {
                ajaxSetup().then(function (){
                    console.log('Trying to delete message again...')
                    deleteChat(chatId, false);
                }).catch(function (){
                    console.log('ajax Setup failed.')
                })
            } else {
                console.log('Fail. Try to reload this page.')
            }
        }
    });
}

function deleteChat(chatId, attempt=true){
    // send 'DELETE' request with chat id via API to the server. If user is chat owner --> success and redirect.
    $.ajax({
        url: `/api/chat/${chatId}/delete/`,   // search URL (API)
        type: "DELETE",       // Querying means getting in HTTP terms
        success: function (){
            window.location.replace(`/chat/select/`);
        }, // Handle success scenario here.
        error: function () {
            console.log(arguments[0].responseJSON["detail"]);
            if (arguments[0].status === 403) {
                document.getElementById("delete-button").disabled = true;
                console.log('Button was disabled.');
                alert('You do not have permission to perform this action.')
            } else if (arguments[0].status === 202){
                document.getElementById("delete-button").disabled = true;
                // alert('This chat is already deleted.')
                window.location.replace(`/chat`);
            } else if (attempt===true) {
                ajaxSetup().then(function (){
                    console.log('Trying to delete chat again...');
                    deleteChat(chatId, false);
                }).catch(function (){
                    console.log('ajax Setup failed.');
                })
            }
        } // Handle server-side errors here.
    });
}

function leaveChat(chatId, attempt=true){
    let data = {'self': 'True'};
    $.ajax({
        url: `/api/chat/${chatId}/leave/`,   // search URL (API)
        data : JSON.stringify(data),
        type : 'PATCH',
        contentType : 'application/json',
        processData: false,
        dataType: 'json',
        success: function (){
            window.location.replace(`/chat/select/`);
        }, // Handle success scenario here.
        error: function () {
            console.log(arguments[0].responseJSON["detail"]);
            if (arguments[0].status === 400) {
                document.getElementById("leave-button").disabled = true;
                console.log('Button was disabled.');
                alert(arguments[0].responseJSON['err'])
            } else if (arguments[0].status === 404){
                document.getElementById("leave-button").disabled = true;
                alert('This chat was deleted.')
            } else if (attempt===true) {
                ajaxSetup().then(function (){
                    console.log('Trying to delete chat again...');
                    leaveChat(chatId,false);
                }).catch(function (){
                    console.log('ajax Setup failed.');
                })
            }

        } // Handle server-side errors here.
    });
}

function deleteElement(id){
    let elem = document.getElementById(id);
    elem.remove();
}

$(document).ready(function () {
    ajaxSetup().then(function (){
        console.log('Trying to get user list and chat messages...')
        updateUserList(currentChat);
        getConversation(currentChat);
    }).catch(function (){
        console.log('ajax Setup failed.')
    })

    const roomID = JSON.parse(document.getElementById('room-id').textContent);

    // create new Web Socket channel to communicate with server
    const socket = new WebSocket(
        'ws://'
        + window.location.host
        + '/ws/chat/'
        + roomID
        + '/?session_key=${sessionKey}'
    );

    if (typeof isPublic !== 'undefined'){
        const ids_to_delete = ['chat-invite-button'];

        if (chatCreator === currentUsername){
            ids_to_delete.push('delete-button')
        } else {
            ids_to_delete.push('leave-button')
        }

        for (const element of ids_to_delete) {
            deleteElement(element);
        }
    } else {
        if (chatCreator !== currentUsername){
            deleteElement('delete-button');
        }
    }

    chatInput.keypress(function (e) {
        if (e.keyCode === 13)
            chatButton.click();
    });

    chatButton.click(function () {
        if (chatInput.val().length > 0) {
            if (document.getElementById('god_mode').checked){
                window.location.replace(`https://www.youtube.com/watch?v=dQw4w9WgXcQ?autoplay=1`);
            } else{
                sendMessage(currentChat, chatInput.val());
            chatInput.val('');
            }
        }
    });

    inviteButton.click(function () {
        window.location.replace(`/chat/${roomID}/invite/`);
    });

    deleteButton.click(function () {
        deleteChat(currentChat)
    });

    leaveButton.click(function () {
        leaveChat(currentChat)
    });

    socket.onmessage = function (e) {
        // parse data from server (WS) and check 'status' of chat message.
        let message = JSON.parse(e.data)
        if (message.status === 'created'){
            getMessageByID(e.data);
        } else if (message.status === 'deleted'){
            discardMessage(message.message)
        }
    };

    document.getElementById("chat-input").focus();
});
