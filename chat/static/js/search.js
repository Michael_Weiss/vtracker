import {ajaxSetup} from "./login.js";

const csrftoken = Cookies.get('csrftoken');
let searchList = $('#search-list');
let searchInput = $('#search-input');
var invitee = [];

$.ajaxSetup({
    headers: {
        'X-CSRFToken': csrftoken
    }
});

function fillInviteeList(){
    // fill JS list with all usernames from 'invitee-list'.
    invitee = [];
    let inviteeNum = document.getElementById('invitee-list').children;
    for (let i = 0; i < inviteeNum.length; i++){
        invitee.push(inviteeNum[i].innerText);
    }
}

function inviteUsers(users, chatId){
    // send update request to backend and if success --> redirect to chat page.
    let data = {'users': users};
    let request = $.ajax({
        url : `/api/chat/${chatId}/`,
        data : JSON.stringify(data),
        type : 'PATCH',
        contentType : 'application/json',
        processData: false,
        dataType: 'json'
    });
    request.fail(function() {
        alert( "Request failed! Reload this page and try again." );
    });
    request.done(function() {
        let roomName = JSON.parse(document.getElementById('room-name').textContent);
        window.location.replace(`/chat/${roomName}/`);
    });
}

function makeMovable(){
    // Add each card an ability to move to another list when is pressed.
    let x = document.getElementById("search-list").children;

    // iterate all 'search-list' children objects and add '.onclick' to them.
    for (let i = 0; i < x.length; i++) {
        x[i].onclick=function(){
            // if user item in search list -> move to invitee list and vice versa.
            if (this.parentNode.id === "search-list"){
                $(this).addClass('list-group-item list-group-item-action active');
                document.getElementById('invitee-list').appendChild(this);
            } else if (this.parentNode.id === "invitee-list") {
                $(this).removeClass('list-group-item list-group-item-action active').addClass('list-group-item list-group-item-action');
                document.getElementById('search-list').appendChild(this);
            }
        }
    }
}

function drawUser(user, empty=false){
    // Draw button with username to the list and append it to the list.
    let userItem;
    if (empty === true){
        userItem = `
        <li class="list-group-item">
            No users found.
        </li>`;
    } else {
        userItem = `
        <button type="button" class="list-group-item list-group-item-action">
            ${user.username}
        </button>`;

    }
    $(userItem).appendTo('#search-list');
}

function search(chatId, attempt=true){
    // If server response is correct - loop retrieved data to draw it.
    searchList.children().remove();
    $.ajax({
        url: `/api/chat/${chatId}/search/`,   // search URL (API)
        type: "get",       // Querying means getting in HTTP terms
        data: {'q': searchInput.val()},    // This transforms your search form into a JSON dictionary.
        success: function (data){
            if (data.length < 1){
                drawUser(data[0], true);
            } else {
                fillInviteeList();
                for (let i = 0; i < data.length; i++) {
                    if (invitee.indexOf(data[i].username) < 0) {
                        drawUser(data[i]);
                    }
                }
                makeMovable();
            }
        },
        error: function () {
            console.log(arguments[0].responseJSON["detail"]);
            if (attempt===true){
                ajaxSetup().then(function (){
                    console.log('Trying to search users again...')
                    search(chatId, false);
                }).catch(function (){
                    console.log('ajax Setup failed.')
                })
            } else {
                console.log('Unable to retrieve users from the server. Please reload the page. Details:');
            }
        } // Handle server-side errors here.
    });
}


$( document ).ready(function() {
    console.log('Initial AJAX setup...')
    ajaxSetup().then(function (){
        console.log('Access token is refreshed.')
    }).catch(function (){
        console.log('ajax Setup failed.')
    })

    const chatId = JSON.parse(document.getElementById('room-id').textContent);
    searchInput.bind('input', function () {
        // If input is changed - search users.
        console.log('Trying to search users...')
        search(chatId);
    })
    $("#invite-button").click(function () {
        fillInviteeList();
        inviteUsers(invitee, chatId);
    })

    document.getElementById("search-input").focus();
});