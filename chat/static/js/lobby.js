import {ajaxSetup} from './login.js';

let createButton = $('#chat-create-btn');
let createPubButton = $('#chat-pub-create-btn');
let chatCreateInput = $('#chat-create-input')

function createChat(name, is_public=false, attempt=true){
    $.post(`/api/chat/`, {
        name: name,
        is_public: is_public,
    })
        .done(function() {
            window.location.replace(`/chat/${name}`);
        })
        .fail(function () {
            if (attempt===true){
                console.log(arguments[0].responseJSON["detail"]);
                ajaxSetup().then(function (){
                    console.log('Trying to create chat again...');
                    createChat(name, is_public, false);
                }).catch(function (){
                    console.log('ajax Setup failed.');
                })
            } else {
                console.log('chat creation failed.');
                alert(arguments[0].responseJSON.name[0]);
            }
        });
}

function drawChatCard(chat){
    if (chat.is_public == false) {
        const chatItem = `
            <a href="/chat/${chat.name}/" class="list-group-item list-group-item-action list-group-item-primary flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">${chat.name}</h5>
                    <small>${chat.users.length} user(s)</small>
                </div>
                <small>Creator: ${chat.creator}</small>
            </a>`;
        $(chatItem).appendTo('#chat-list');
    } else {
        const chatItem = `
            <a href="/chat/${chat.name}/" class="list-group-item list-group-item-action list-group-item-success flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">${chat.name}</h5>
                    <small><strong>public</strong></small>
                </div>
                <small>Creator: ${chat.creator}</small>
            </a>`;
        $(chatItem).appendTo('#chat-list');
    }
}

function getChatList(){
        $.getJSON(`/api/chats/`, function (data) {
        for (let i = 0; i < data.length; i++) {
            drawChatCard(data[i]);
        }
    });
}

$( document ).ready(function() {
    console.log( "Document " + "lobby.js" + " is ready!" );
    ajaxSetup().then(function (){
        console.log('Trying to get chat list...')
        getChatList();
    }).catch(function (){
        console.log('ajax Setup failed.')
    })


    createButton.click(function () {
        if (chatCreateInput.val().length > 0) {
            createChat(chatCreateInput.val());
        }
    });
    createPubButton.click(function () {
        if (chatCreateInput.val().length > 0) {
            createChat(chatCreateInput.val(), true);
        }
    });
});
