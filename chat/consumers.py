import json
from channels.generic.websocket import AsyncWebsocketConsumer


class ChatConsumer(AsyncWebsocketConsumer):
    """"""
    # TODO: add mods to user (before user connects to the room he can choose one of the mods)
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']  # url parameter from client
        self.room_group_name = f'{self.room_name}'

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        text_data_json['is_admin'] = self.scope['user'].is_superuser
        text_data_json['username'] = self.scope['user'].username
        message = text_data_json

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat.message',
                'message': message,
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        status = event['status']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'status': status,
        }))
