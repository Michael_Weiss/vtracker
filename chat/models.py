from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_delete
from django.dispatch import receiver

from accounts.models import Profile
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync


class GroupModel(models.Model):
    """Chat model. Can be public and private."""
    name = models.CharField(max_length=20, null=True)
    creator = models.ForeignKey(User, related_name='creator', null=True, on_delete=models.SET_NULL)
    users = models.ManyToManyField(User, related_name='users', blank=True)
    is_public = models.BooleanField(default=False, blank=True, null=True)


class MessageModel(models.Model):
    """Message model."""
    message = models.CharField(max_length=200)
    sender = models.ForeignKey(User, related_name='sender', null=True, on_delete=models.SET_NULL)
    recipient = models.ForeignKey(GroupModel, related_name='chat', null=False, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True, editable=False)

    def notify_ws_clients(self):
        """Notify all channel clients that message was deleted."""
        channel_layer = get_channel_layer()
        notification = {'type': 'chat.message', 'message': f'{self.id}', 'status': 'created'}
        async_to_sync(channel_layer.group_send)(f'{self.recipient.id}', notification)
        async_to_sync(channel_layer.group_send)(f'{self.sender.id}', notification)

    def save(self, *args, **kwargs):
        """If message is new --> notify all chat members."""
        new = self.id
        super(MessageModel, self).save(*args, **kwargs)
        if new is None:
            self.notify_ws_clients()


# run function when some instance of MessageModel was deleted.
@receiver(post_delete, sender=MessageModel)
def lol(instance, **kwargs):
    """When some message was deleted --> send WS notice to all channel users with status 'deleted' and message id."""
    print('Message is deleted!')
    channel_layer = get_channel_layer()
    notification = {'type': 'chat.message', 'message': f'{instance.id}', 'status': 'deleted'}
    async_to_sync(channel_layer.group_send)(f'{instance.recipient.id}', notification)
    async_to_sync(channel_layer.group_send)(f'{instance.sender.id}', notification)
