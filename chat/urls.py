from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import (
    login,
    chat_enter,
    chat_select,
    chat_create,
    chat_invite,
)


urlpatterns = [
    path('', chat_select, name='chats'),
    path('login/', login_required(login, login_url='/accounts/login/'), name='login-chat'),
    path('select/', login_required(chat_select, login_url='/accounts/login/'), name='chat-select'),
    path('<str:room_name>/', login_required(chat_enter, login_url='/accounts/login/'), name='chat-enter'),
    path('<str:room_name>/create/', login_required(chat_create, login_url='/accounts/login/'), name='chat-create'),
    path('<str:room_id>/invite/', login_required(chat_invite, login_url='/accounts/login/'), name='chat-invite'),
]
