from django import forms
from bootstrap_datepicker_plus import DatePickerInput
from .models import Task, Date, Comment
from datetime import date
import logging

logger = logging.getLogger(__name__)


class DateModelForm(forms.ModelForm):
    class Meta:
        model = Date
        fields = [
            'start_date',
            'end_date',
            'status',
        ]
        widgets = {
            'start_date': DatePickerInput(
                options={
                    "format": "YYYY-MM-DD",  # date-time format.
                    "showClose": True,
                    "showClear": False,
                    "showTodayButton": True,
                }
            ),
            'end_date': DatePickerInput(
                options={
                    "format": "YYYY-MM-DD",  # date-time format.
                    "showClose": True,
                    "showClear": False,
                    "showTodayButton": True
                }
            ),
            'status': forms.Select(
                attrs={'class': 'form-control'}
            ),
        }

    def clean(self):
        """Compare filter dates and if start_date > end_date --> form is not valid."""
        stt_date = self.cleaned_data.get('start_date')
        end_date = self.cleaned_data.get('end_date')
        if stt_date is not None and end_date is not None:
            if stt_date > end_date:
                logger.info(f"Invalid date. {stt_date} > {end_date} is not allowed.")
                raise forms.ValidationError('Start date may not be bigger than end date.')


class TaskModelForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            'title',
            'assignee',
            'description',
            'status',
            'deadline',
            'attachment',
        ]
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'assignee': forms.Select(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'status': forms.Select(attrs={'class': 'form-control'}),
            'deadline': DatePickerInput(options={"format": "YYYY-MM-DD"}),
        }

    def clean_title(self):
        """Task title must have at least 4 characters"""
        data_title = self.cleaned_data.get('title')
        if len(data_title) < 4:  # set minimal number of characters here.
            raise forms.ValidationError("This is not long enough.")
        return data_title

    def clean_deadline(self):
        """If deadline date is lesser than current date --> deadline is not valid."""
        deadline = self.cleaned_data.get('deadline')
        if deadline is not None:
            if deadline < date.today():
                logger.info(f"Invalid deadline. {deadline} < {date.today()} is not allowed.")
                raise forms.ValidationError('Deadline date may not be lower than current date.')
            return deadline


class CommentModelForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = [
            'comment',
        ]
        widgets = {
            'comment': forms.TextInput(attrs={'class': 'form-control'}),
        }

    def clean_comment(self):
        """Comment must have at least 2 characters."""
        comment_text = self.cleaned_data.get('comment')
        if len(comment_text) < 2:  # set minimal number of characters here.
            logger.info(f"Invalid comment. {len(comment_text)} characters, but must me more then 2.")
            raise forms.ValidationError("This is not long enough.")
        return comment_text
