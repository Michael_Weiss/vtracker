from django.test import TestCase
from django.contrib.auth.models import User
from datetime import date, timedelta
import pathlib

from django.urls import reverse

from .models import Task, Comment

# TODO: date filter test


class TaskCreateTestCase(TestCase):

    def setUp(self):
        """
        Initial create of user instance.
        """
        # create new user and check if it was redirected.
        self.initUsr = 'test'
        self.initEmail = 'test@mail.com'
        self.initPwd = 'test'
        self.user = User.objects.create_user(username=self.initUsr, email=self.initEmail, password=self.initPwd)
        self.user.save()
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.url, '/')

        # create assignee user.
        assignee_usr = 'assignee'
        assignee_email = 'assignee@mail.com'
        assignee_pwd = 'assignee'
        self.assignee = User.objects.create_user(username=assignee_usr, email=assignee_email, password=assignee_pwd)
        self.assertEqual(self.assignee.username, assignee_usr)

    def test_task_create_correct(self):
        """"""
        title = 'Test'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'to do'
        deadline = date.today()
        path = str(pathlib.Path().absolute())
        with open(path + '/cdn_test/media/test.jpg', 'rb') as attachment:
            response = self.client.post(
                '/task/create/',
                {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
                 'deadline': deadline, 'attachment': attachment})
        task = Task.objects.last()

        # comparisons and checks
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(task.title, title)

    def test_task_create_incorrect_title(self):
        """"""
        title = '1'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'to do'
        deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertIsNone(task)

    def test_task_create_incorrect_assignee(self):
        """"""
        title = 'Test'
        assignee_id = 404
        description = 'Test'
        status = 'to do'
        deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertIsNone(task)

    def test_task_create_incorrect_deadline(self):
        """"""
        title = 'Test'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'to do'
        deadline = date.today() - timedelta(days=365)
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertIsNone(task)

    def test_task_create_incorrect_status(self):
        """"""
        title = 'Test'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'Error'
        deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertIsNone(task)

    def test_task_create_incorrect_attachment(self):
        """"""
        title = 'Test'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'to do'
        deadline = date.today()
        attachment = 'error'
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline, 'attachment': attachment})
        task = Task.objects.last()

        # comparisons and checks
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(task.title, title)

    def test_task_create_empty_title_error(self):
        """"""
        title = ''
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'to do'
        deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertIsNone(task)

    def test_task_create_empty_assignee_error(self):
        """"""
        title = 'Test'
        assignee_id = ''
        description = 'Test'
        status = 'to do'
        deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertIsNone(task)

    def test_task_create_empty_status_error(self):
        """"""
        title = 'Test'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = ''
        deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertIsNone(task)

    def test_task_create_empty_description_is_ok(self):
        """"""
        title = 'Test'
        assignee_id = self.assignee.pk
        description = ''
        status = 'to do'
        deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(response.status_code, 200)
        self.assertIsNotNone(task)

    def test_task_create_empty_deadline_is_ok(self):
        """"""
        title = 'Test'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'to do'
        deadline = ''
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(response.status_code, 200)
        self.assertIsNotNone(task)

    def test_task_create_empty_attachment_is_ok(self):
        """"""
        title = 'Test'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'to do'
        deadline = date.today()
        attachment = ''
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline, 'attachment': attachment})
        task = Task.objects.last()

        # comparisons and checks
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(response.status_code, 200)
        self.assertIsNotNone(task)


class TaskEditTestCase(TestCase):

    def setUp(self):
        """
        Initial create of user instance.
        """
        # create new user and check if it was redirected.
        self.initUsr = 'test'
        self.initEmail = 'test@mail.com'
        self.initPwd = 'test'
        self.user = User.objects.create_user(username=self.initUsr, email=self.initEmail, password=self.initPwd)
        self.user.save()
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.url, '/')

        # create assignee user.
        assignee_usr = 'assignee'
        assignee_email = 'assignee@mail.com'
        assignee_pwd = 'assignee'
        self.assignee = User.objects.create_user(username=assignee_usr, email=assignee_email, password=assignee_pwd)
        self.assertEqual(self.assignee.username, assignee_usr)

    def test_task_edit_correct(self):
        """"""
        # create task
        title = 'Test'
        assignee_id = self.assignee.pk
        description = 'Test'
        status = 'to do'
        deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(task.title, title)

        # edit task
        task_id = task.pk
        title = 'Test (edited)'
        assignee_id = self.user.pk
        description = 'Test (edited)'
        status = 'completed'
        deadline = date.today() + timedelta(days=31)
        response = self.client.post(
            f'/task/{task_id}/edit/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        task = Task.objects.last()

        # comparisons and checks
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(task.title, title)
        self.assertEqual(task.assignee_id, assignee_id)
        self.assertEqual(task.description, description)
        self.assertEqual(task.status, status)
        # self.assertEqual(task.deadline, deadline)


class TaskPermissionsTestCase(TestCase):

    def setUp(self):
        """
        Initial create of user instance.
        """
        # create new user and check if it was redirected.
        init_usr = 'test'
        init_email = 'test@mail.com'
        init_pwd = 'test'
        self.user = User.objects.create_user(username=init_usr, email=init_email, password=init_pwd)
        self.user.save()

        # create assignee user.
        assignee_usr = 'assignee'
        assignee_email = 'assignee@mail.com'
        assignee_pwd = 'assignee'
        self.assignee = User.objects.create_user(username=assignee_usr, email=assignee_email, password=assignee_pwd)
        self.assertEqual(self.assignee.username, assignee_usr)

        # create another user.
        hacker_usr = 'hacker'
        hacker_email = 'hacker@mail.com'
        self.hacker_pwd = 'hacker'
        self.hacker = User.objects.create_user(username=hacker_usr, email=hacker_email, password=self.hacker_pwd)
        self.assertEqual(self.hacker.username, hacker_usr)

        # log in as 'test' user and create task to 'assignee' and log out.
        response = self.client.post('/accounts/login/', {'username': init_usr, 'password': init_pwd})
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.url, '/')

        # create task.
        self.title = 'Test'
        self.assignee_id = self.assignee.pk
        self.description = 'Test'
        self.status = 'to do'
        self.deadline = date.today()
        response = self.client.post(
            '/task/create/',
            {'title': self.title, 'assignee': self.assignee_id, 'description': self.description, 'status': self.status,
             'deadline': self.deadline})

        # check if task is ok.
        self.task = Task.objects.last()
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(self.task.user_id, self.user.pk)
        self.assertEqual(self.task.assignee_id, self.assignee.pk)

        # create comment.
        self.client.post(f'/task/comment/{self.task.id}/create/', {'comment': 'Test'})
        self.comment = Comment.objects.last()
        self.assertIsNotNone(self.comment)

        # user log out.
        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(response.status_code, 200)
        #
        # hacker log in.
        response = self.client.post('/accounts/login/', {'username': self.hacker.username, 'password': self.hacker_pwd})
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(response.url, '/')

    def test_disallowed_task_show_403(self):
        # trying to see disallowed task --> 403.
        task_id = self.task.id
        response = self.client.get(f'/task/{ task_id }/preview/')
        self.assertEqual(response.status_code, 403)

    def test_disallowed_task_edit_403(self):
        # trying to edit disallowed task --> 403.
        task_id = self.task.id
        title = 'Hacker test'
        assignee_id = self.hacker.id
        description = 'Hacker test'
        status = 'complete'
        deadline = date.today() + timedelta(days=31)
        response = self.client.put(f'/task/{task_id}/edit/',
            {'title': title, 'assignee': assignee_id, 'description': description, 'status': status,
             'deadline': deadline})
        hacked_test = Task.objects.get(pk=self.task.pk)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(hacked_test.user, self.task.user)
        self.assertEqual(hacked_test.title, self.task.title)
        self.assertEqual(hacked_test.assignee, self.task.assignee)
        self.assertEqual(hacked_test.description, self.task.description)
        # self.assertNotEqual(hacked_test.deadline, self.task.deadline)

    def test_disallowed_task_delete_403(self):
        # trying to delete disallowed task --> 403.
        task_id = self.task.id
        response = self.client.get(f'/task/{task_id}/delete/')
        self.assertEqual(response.status_code, 403)

    def test_disallowed_task_status_change_403(self):
        # trying to edit status of disallowed task --> 403.
        task_id = self.task.id
        new_status = 'completed'
        response = self.client.get(f'/task/{task_id}/newstatus/{new_status}/')
        self.assertEqual(response.status_code, 403)
        self.assertNotEqual(self.task.status, new_status)

    def test_disallowed_comment_create_403(self):
        """"""
        task_id = self.task.id
        hacker_comment = 'Hacker text'
        response = self.client.post(f'/task/comment/{task_id}/create/', {'comment': hacker_comment})
        comment = Comment.objects.filter(comment=hacker_comment).count()
        self.assertEqual(comment, 0)
        self.assertEqual(response.status_code, 403)

    def test_disallowed_comment_edit_403(self):
        """"""
        task_id = self.task.id
        hacker_comment = 'Hacker text (edited)'
        response = self.client.post(f'/task/comment/{task_id}/edit/', {'comment': hacker_comment})
        comment = Comment.objects.get(pk=self.comment.pk)
        self.assertNotEqual(comment.comment, hacker_comment)
        self.assertEqual(response.status_code, 403)

    def test_disallowed_comment_delete_403(self):
        """"""
        commend_id = self.comment.id
        response = self.client.get(f'/task/comment/{commend_id}/delete/')
        comment = Comment.objects.get(pk=commend_id)
        self.assertIsNotNone(comment)
        self.assertEqual(response.status_code, 403)


class TaskDetailTestCase(TestCase):
    def setUp(self):
        """"""
        # create first user.
        self.username = 'test'
        self.password = 'test'
        self.email = 'test@test.com'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)
        self.user.save()
        self.assertEqual(User.objects.count(), 1)

        # create second user.
        self.username2 = 'hacker'
        self.password2 = 'hacker'
        self.email2 = 'hacker@test.com'
        self.user2 = User.objects.create_user(username=self.username2, email=self.email2, password=self.password2)
        self.user2.save()
        self.assertEqual(User.objects.count(), 2)

        # create task.
        title = 'test'
        self.task = Task.objects.create(title=title, user_id=self.user.id, assignee_id=self.user.id)
        self.task.save()
        self.assertEqual(Task.objects.count(), 1)

    def test_task_detail(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # get task preview.
        task_pk = self.task.pk
        url = reverse('task-detail', kwargs={'pk': task_pk})
        response = self.client.get(url)

        # check if all is ok.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['object'].title, self.task.title)

    def test_task_detail_bad_task_id(self):
        """"""
        # log in.
        self.client.login(username=self.username, password=self.password)

        # get task preview.
        task_pk = 404  # bad task id
        url = reverse('task-detail', kwargs={'pk': task_pk})
        response = self.client.get(url)

        # check if all is ok.
        self.assertEqual(response.status_code, 404)

    def test_task_detail_disallowed(self):
        """"""
        # log in.
        self.client.login(username=self.username2, password=self.password2)

        # get task preview.
        task_pk = self.task.pk
        url = reverse('task-detail', kwargs={'pk': task_pk})
        response = self.client.get(url)

        # check if all is ok.
        self.assertEqual(response.status_code, 403)


class TaskHistoryTestCase(TestCase):
    """"""
    def setUp(self):
        """
        Initial create of user instance.
        """
        # create new user and check if it was redirected.
        init_usr = 'test'
        init_email = 'test@mail.com'
        self.init_pwd = 'test'
        self.user = User.objects.create_user(username=init_usr, email=init_email, password=self.init_pwd)
        self.user.save()

        # create another user.
        hacker_usr = 'hacker'
        hacker_email = 'hacker@mail.com'
        self.hacker_pwd = 'hacker'
        self.hacker = User.objects.create_user(username=hacker_usr, email=hacker_email, password=self.hacker_pwd)
        self.assertEqual(self.hacker.username, hacker_usr)

        # create task and check if task is ok.
        title = 'test'
        assignee_id = self.user.id
        self.task = Task.objects.create(title=title, assignee_id=assignee_id, user_id=self.user.id)
        self.task.save()

        self.assertEqual(Task.objects.count(), 1)
        self.assertEqual(self.task.user_id, self.user.pk)
        self.assertEqual(self.task.assignee_id, self.user.pk)

        # create comment and check if comment is ok.
        comment_msg = 'test'
        self.comment = Comment.objects.create(comment=comment_msg)
        self.comment.save()

        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(self.comment.comment, comment_msg)

        # edit comment.
        ed_comment_msg = 'test (edited)'
        self.comment.comment = ed_comment_msg
        self.comment.save()

        # edit task.
        ed_title = 'test (edited)'
        self.task.title = ed_title
        self.task.save()

    def test_get_history(self):
        """"""
        self.client.login(username=self.user.username, password=self.init_pwd)
        task_id = self.task.pk
        url = reverse('task-history', kwargs={'pk': task_id})
        response = self.client.get(url)

        # comparisons and checks
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.template_name[0], 'task_history.html')

    def test_get_history_disallowed(self):
        """"""
        self.client.login(username=self.hacker.username, password=self.hacker_pwd)
        task_id = self.task.pk
        url = reverse('task-history', kwargs={'pk': task_id})
        response = self.client.get(url)

        # comparisons and checks
        self.assertEqual(response.status_code, 403)

    def test_get_history_bad_task_pk(self):
        """"""
        self.client.login(username=self.hacker.username, password=self.hacker_pwd)
        task_id = 404
        url = reverse('task-history', kwargs={'pk': task_id})
        response = self.client.get(url)

        # comparisons and checks
        self.assertEqual(response.status_code, 404)


class CommentTestCase(TestCase):
    """"""
    def setUp(self):
        """"""
        pass
