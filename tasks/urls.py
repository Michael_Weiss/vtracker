from django.urls import path
from django.contrib.auth.decorators import login_required


from .views import (
    TaskCreate,
    TaskDetailView,
    TaskEdit,
    TaskPreview,
    TaskList,
    TaskDelete,
    TaskHistory,
    CommentCreate,
    CommentDelete,
    CommentEdit,
)

urlpatterns = [
    path('create/', TaskCreate.as_view(), name='task-add'),
    path('<int:pk>/', TaskDetailView.as_view(), name='task-detail'),
    path('<int:pk>/preview/', TaskPreview.as_view(), name='task-preview'),
    path('<int:pk>/edit/', TaskEdit.as_view(), name='task-edit'),
    path('<int:pk>/delete/', TaskDelete.as_view(), name='task-delete'),
    path('<int:pk>/history/', TaskHistory.as_view(), name='task-history'),
    path('<int:pk>/newstatus/<str:status>/', login_required(TaskList.as_view()), name='task-status'),
    path('comment/<int:pk>/create/', CommentCreate.as_view(), name='comment-create'),
    path('comment/<int:pk>/edit/', CommentEdit.as_view(), name='comment-edit'),
    path('comment/<int:pk>/delete/', CommentDelete.as_view(), name='comment-delete'),
]
