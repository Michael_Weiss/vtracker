import os
import operator
import logging
from itertools import chain
from functools import reduce as red

import diff_match_patch as dmp_module

from django.db.models import Q
from django.http import Http404
from django.shortcuts import render
from django.urls import reverse_lazy
from django.core.paginator import Paginator
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView
from django.views.generic.edit import DeleteView, CreateView, UpdateView

from .models import Task, Comment
from .forms import TaskModelForm, DateModelForm, CommentModelForm

logger = logging.getLogger(__name__)


class TaskList(ListView):
    """Send queryset with tasks to template to list them, depending on filters."""
    model = Task
    template_name = 'home.html'
    paginate_by = 8  # items per page.

    def get_context_data(self, *, object_list=None, **kwargs):
        form = DateModelForm(self.request.GET)
        # messages.info(self.request, f"Hello, {self.request.user}")
        if form.is_valid():
            queryset = object_list if object_list is not None else self.object_list
            # user can edit task status on the fly when
            if 'status' in self.kwargs:
                task_status = self.kwargs['status']
                task_pk = self.kwargs['pk']
                q = Task.objects.get(pk=task_pk)
                if task_status in ['to do', 'in progress', 'completed'] and (q.assignee_id is self.request.user.id or q.user_id is self.request.user.id):
                    q.status = task_status
                    q.save()
                else:
                    logger.info(f"User {self.request.user.username} tried to change status of task {task_pk}. Err 403.")
                    raise PermissionDenied()
            start_date = form.cleaned_data.get('start_date')
            end_date = form.cleaned_data.get('end_date')
            queryset_list = [Q(assignee=self.request.user)]
            assigned_list = [Q(user=self.request.user), ~Q(assignee=self.request.user)]
            if start_date is not None and end_date is not None and start_date < end_date:
                queryset_list.append(Q(creation_date__range=[start_date, end_date]))
                assigned_list.append(Q(creation_date__range=[start_date, end_date]))
            status = form.cleaned_data.get('status')
            if status in ['to do', 'in progress', 'completed']:
                queryset_list.append(Q(status=status))
                assigned_list.append(Q(status=status))
            assigned = queryset.filter(red(operator.and_, assigned_list))
            queryset = queryset.filter(red(operator.and_, queryset_list))

            # return last 8 assigned tasks.
            assigned = Paginator(assigned, 8).get_page(1)
        else:
            # FIXME
            return super().get_context_data(form=form)

        return super().get_context_data(
            form=form,  # Send ModelForm to template.
            assigned_list=assigned,
            object_list=queryset,
            **kwargs)


class CommentCreate(LoginRequiredMixin, CreateView):
    """Send comment creation form to template and save it after receive user response."""
    model = Comment
    form_class = CommentModelForm
    template_name = 'forms.html'
    login_url = '/accounts/login/'

    def form_valid(self, form, **kwargs):
        task = Task.objects.get(pk=self.kwargs.get('pk'))
        if self.request.user.id not in [task.assignee_id, task.user_id]:
            logger.info(f"User {self.request.user.username} tried to create comment for task {task.pk}. Err 403.")
            raise PermissionDenied()

        form.instance.commentator = self.request.user
        form.instance.task = task
        return super(CommentCreate, self).form_valid(form)


class TaskCreate(LoginRequiredMixin, CreateView):
    """Send task creation form to template and save it after receive user response."""
    model = Task
    form_class = TaskModelForm
    template_name = 'forms.html'
    login_url = '/accounts/login/'

    def form_valid(self, form):
        form.instance.user = self.request.user  # set request.user to form "user" field.
        return super().form_valid(form)


class TaskPreview(LoginRequiredMixin, DetailView):
    """If user has permissions, send task data to template, depending on URL 'pk'."""
    model = Task
    template_name = 'preview.html'
    context_object_name = 'Tasks'
    login_url = '/accounts/login/'

    def get_object(self, *args, **kwargs):
        """get task object if user is assignee or owner."""
        obj = super(TaskPreview, self).get_object(*args, **kwargs)
        if not obj.user == self.request.user:
            logger.info(f"User {self.request.user.username} tried to see preview of forbidden task {obj.pk}. Err 403.")
            raise PermissionDenied()
        return obj

    def get_context_data(self, **kwargs):
        """send task and its comments to the template."""
        context = super().get_context_data(**kwargs)
        context['info'] = Task.objects.get(pk=self.kwargs.get('pk'))
        return context


class TaskDetailView(LoginRequiredMixin, DetailView):
    """If user has permissions, send task data with comments to template, depending on URL 'pk'."""
    model = Task
    template_name = 'detail_view.html'
    context_object_name = 'Tasks'
    login_url = '/accounts/login/'

    def get_object(self, *args, **kwargs):
        """get task object if user is assignee or owner."""
        obj = super(TaskDetailView, self).get_object(*args, **kwargs)
        if not obj.user == self.request.user and not obj.assignee == self.request.user:
            logger.info(f"User {self.request.user.username} tried to see forbidden task {obj.pk}. Err 403.")
            raise PermissionDenied()
        return obj

    def get_context_data(self, **kwargs):
        """send task and its comments to the template."""
        context = super().get_context_data(**kwargs)
        context['info'] = Task.objects.get(pk=self.kwargs.get('pk'))
        context['comments'] = Comment.objects.filter(task__pk=self.kwargs.get('pk'))
        return context


class TaskEdit(LoginRequiredMixin, UpdateView):
    """If user has permission, send task edit form and if form is valid, save it."""
    model = Task
    form_class = TaskModelForm
    template_name = 'forms.html'
    login_url = '/accounts/login/'

    def get_object(self, *args, **kwargs):
        obj = super(TaskEdit, self).get_object(*args, **kwargs)
        if not obj.user == self.request.user:
            logger.info(f"User {self.request.user.username} tried to edit forbidden task {obj.pk}. Err 403.")
            raise PermissionDenied()
        return obj

    def form_valid(self, form):
        form.instance.user = self.request.user  # request.user to --> form.
        # if user has updated attachment -> delete old attachment if any.
        if 'attachment' in form.changed_data and form.initial['attachment'].name != '':
            old_att_path = form.initial['attachment'].path
            os.remove(old_att_path)
        return super().form_valid(form)


class CommentEdit(LoginRequiredMixin, UpdateView):
    """If user has permission, send comment edit form and if form is valid, save it."""
    model = Comment
    form_class = CommentModelForm
    template_name = 'forms.html'
    login_url = '/accounts/login/'

    def get_object(self, *args, **kwargs):
        obj = super(CommentEdit, self).get_object(*args, **kwargs)
        if not obj.commentator == self.request.user:
            logger.info(f"User {self.request.user.username} tried to see forbidden comment {obj.pk}. Err 403.")
            raise PermissionDenied()
        return obj

    def form_valid(self, form):
        form.instance.user = self.request.user  # request.user to --> form.
        return super().form_valid(form)


class TaskDelete(LoginRequiredMixin, DeleteView):
    """If user has permission to delete task, send 'confirm delete' page."""
    model = Task
    success_url = reverse_lazy('task-all')
    login_url = '/accounts/login/'

    def get_object(self, *args, **kwargs):
        obj = super(TaskDelete, self).get_object(*args, **kwargs)
        if not obj.user == self.request.user:
            logger.info(f"User {self.request.user.username} tried to delete forbidden task {obj.pk}. Err 403.")
            raise PermissionDenied()
        return obj


class CommentDelete(LoginRequiredMixin, DeleteView):
    """If user has permission to delete comment, send 'confirm delete' page."""
    model = Comment
    success_url = reverse_lazy('task-all')
    login_url = '/accounts/login/'

    def get_object(self, *args, **kwargs):
        obj = super(CommentDelete, self).get_object(*args, **kwargs)
        if not obj.commentator == self.request.user:
            logger.info(f"User {self.request.user.username} tried to delete forbidden comment {obj.pk}. Err 403.")
            raise PermissionDenied()
        return obj


class TaskHistory(LoginRequiredMixin, ListView):
    """Show user a list with task history. That contains comments too."""
    model = Task
    template_name = 'task_history.html'
    login_url = '/accounts/login/'

    def get_queryset(self):
        try:
            task_pk = self.kwargs['pk']
            task = Task.objects.get(pk=task_pk)
        except Task.DoesNotExist:
            raise Http404
        cur_user = self.request.user
        if cur_user in [task.assignee, task.user]:
            return task
        else:
            raise PermissionDenied()

    def get_context_data(self, *, object_list=None, **kwargs):
        """Send history to the template."""
        tasks_dif_qs = []
        comments_dif_qs = []
        tasks_qs = Task.history.filter(id=self.kwargs.get('pk'))
        dmp = dmp_module.diff_match_patch()

        # iterate all history records and search for changes.
        for record in tasks_qs:
            if record.prev_record is not None:
                changes = record.diff_against(record.prev_record)
                for change in changes.changes:
                    if change.field in ['title', 'description']:
                        diff = dmp.diff_main(change.old, change.new)
                        dmp.diff_cleanupSemantic(diff)
                        change.diff = diff
                tasks_dif_qs.append(changes)
            else:
                tasks_dif_qs.append(record.diff_against(record))
        comments_qs = Comment.history.filter(task__pk=self.kwargs.get('pk'))

        # iterate all comments records and search for changes.
        for record in comments_qs:
            if record.prev_record is not None:
                changes = record.diff_against(record.prev_record)
                comments_dif_qs.append(changes)
            else:
                comments_dif_qs.append(record.diff_against(record))
        result_qs = sorted(chain(tasks_dif_qs, comments_dif_qs), key=operator.attrgetter('new_record.history_date'))

        return super().get_context_data(
             obj_qs=result_qs,
             **kwargs)


def sandbox_view(request):
    """Sandbox view for experiments."""
    return render(request, 'tasks/sandbox.html', {})
