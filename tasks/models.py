import os

from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from datetime import date
from django.conf import settings
from simple_history.models import HistoricalRecords

User = settings.AUTH_USER_MODEL


class Date(models.Model):
    start_date = models.DateField('start date', blank=True)
    end_date = models.DateField('end date', blank=True)

    class TaskStatus(models.TextChoices):
        all = 'all'
        to_do = 'to do'
        in_progress = 'in progress'
        completed = 'completed'

    status = models.CharField(max_length=200, choices=TaskStatus.choices, default=TaskStatus.all, blank=True)


class Task(models.Model):
    # pk aka id --> numbers
    user = models.ForeignKey(User, related_name='owner', null=True, on_delete=models.SET_NULL)
    assignee = models.ForeignKey(User, related_name='assignee', null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=500, blank=True)
    attachment = models.FileField(upload_to='uploads/', null=True, blank=True)
    creation_date = models.DateField(default=date.today)
    deadline = models.DateField(null=True, blank=True)
    external_id = models.IntegerField(null=True, blank=True)
    history = HistoricalRecords()
    # is_missed = models.BooleanField(null=True)

    class TaskStatus(models.TextChoices):
        to_do = 'to do'
        in_progress = 'in progress'
        completed = 'completed'

    status = models.CharField(max_length=200, choices=TaskStatus.choices, default=TaskStatus.to_do)

    def get_absolute_url(self):
        return reverse('task-preview', kwargs={'pk': self.pk})


class Comment(models.Model):
    task = models.ForeignKey(Task, related_name='task', null=True, blank=False, on_delete=models.CASCADE)
    comment = models.TextField(max_length=200, blank=True)
    commentator = models.ForeignKey(User, related_name='commentator', null=True, blank=False, on_delete=models.CASCADE)
    ext_task_id = models.IntegerField(null=True, blank=True)
    external_id = models.IntegerField(null=True, blank=True)
    history = HistoricalRecords()

    def get_absolute_url(self):
        return reverse('task-detail', kwargs={'pk': self.task.pk})  # not right


@receiver(models.signals.post_delete, sender=Task)
def delete_file(sender, instance, *args, **kwargs):
    """ Deletes attachment files on `post_delete` """
    if instance.attachment:
        if os.path.isfile(instance.attachment.path):
            # Delete file from filesystem.
            os.remove(instance.attachment.path)
