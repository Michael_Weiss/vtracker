from rest_framework import permissions
from accounts.models import Profile
from tasks.models import Task


class IsOwnerOrAssignee(permissions.BasePermission):
    """
    Permissions for 'Task' model
    If request.user is owner - he can edit task
    If request.user is assignee - he can see task
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS and obj.assignee == request.user:
            return True
        return obj.user == request.user


class CommentCreatePermission(permissions.BasePermission):
    """
    Returns PermissionDenied 403 if request.user is either not task owner or not assigned to it.
    Otherwise allow to see.
    """
    def has_permission(self, request, view):
        task_pk = request.data['task']  # 'view.kwargs.get('pk')' --> 'request.data['task']'
        if request.method in permissions.SAFE_METHODS and task_pk is None:
            return True
        try:
            task = Task.objects.get(pk=task_pk)
        except Task.DoesNotExist:
            task = None
        if task is not None:
            return task.user == request.user or task.assignee == request.user
        else:
            return False


class PictureEditPermission(permissions.BasePermission):
    """
    Returns PermissionDenied 403 if request.user is not Profile owner and tries to update a picture
    """

    def has_permission(self, request, view):
        user = Profile.objects.get(user_id=view.kwargs['pk'])
        return user.user == request.user


class AttachmentEditPermission(permissions.BasePermission):
    """
    Returns PermissionDenied 403 if request.user is not task owner and tries to update an attachment
    """

    def has_permission(self, request, view):
        task = Task.objects.get(pk=view.kwargs['pk'])
        return task.user == request.user


class ProfileEditPermission(permissions.BasePermission):
    """
    Returns PermissionDenied 403 if request.user is not Profile owner and tries to update info
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS and obj.user != request.user:
            return True
        return obj.user == request.user


class UserEditPermission(permissions.BasePermission):
    """
    Returns PermissionDenied 403 if request.user is not User model owner and tries to update it
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS and (
                obj != request.user):
            return True
        return obj == request.user


class CommentRUDPermission(permissions.BasePermission):
    """
    Returns PermissionDenied 403 if request.user is not comment owner if user tries to update it.
    If request.user is assigned to task of comment  - he can retrieve it via GET.
    """

    def has_object_permission(self, request, view, obj):
        if obj.task.assignee == obj.commentator:
            not_owner = obj.task.user
        else:
            not_owner = obj.task.assignee

        if request.method in permissions.SAFE_METHODS and not_owner == request.user:
            return True  # permission to see
        return obj.commentator == request.user  # permission to edit


class TaskCommentsListPermission(permissions.BasePermission):
    """
    Returns PermissionDenied 403 if request.user is either not task owner or not assigned to it.
    """

    def has_permission(self, request, view):
        task_pk = view.kwargs.get('pk')
        task = Task.objects.get(pk=task_pk)
        if request.method in permissions.SAFE_METHODS and (task.user == request.user or task.assignee == request.user):
            return True  # permission to see
        return False  # permission to edit


class CommentListPermission(permissions.BasePermission):
    """
    Returns PermissionDenied 403 if request.method not in SAFE_METHODS.
    """

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True  # permission to see
        return False  # permission to edit
