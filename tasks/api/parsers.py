import unicodecsv as csv
import logging
import zipfile
import csv as pycsv
import io
import os

from django.conf import settings
from rest_framework.parsers import BaseParser
from rest_framework.exceptions import ParseError
from rest_framework_csv.orderedrows import OrderedRows as set_header

logger = logging.getLogger(__name__)


def unicode_csv_reader(csv_data, dialect=csv.excel, charset='utf-8', **kwargs):
    csv_reader = csv.reader(csv_data, dialect=dialect, encoding=charset, **kwargs)
    for row in csv_reader:
        yield row


def universal_newlines(stream):
    # It's possible that the stream was not opened in universal
    # newline mode. If not, we may have a single "row" that has a
    # bunch of carriage return (\r) characters that should act as
    # newlines. For that case, lets call splitlines on the row. If
    # it doesn't have any newlines, it will return a list of just
    # the row itself.
    for line in stream.splitlines():
        # that was hard...! 3 hours spent for this 2 lines...
        if b'\x00' in line:
            line = line.strip(b'\x00')
        yield line


class ZIPCSVParser(BaseParser):
    """
    Unzip .zip file and parse files.

    Parses CSV serialized data and save local files.

    The parser assumes the first line contains the column names.
    """

    media_type = 'application/zip'

    def parse(self, stream, media_type=None, parser_context=None):
        parser_context = parser_context or {}
        delimiter = parser_context.get('delimiter', ',')
        encoding = parser_context.get('encoding', settings.DEFAULT_CHARSET)

        try:
            content = {}

            # open ZIP file.
            zip_stream_io = io.BytesIO(stream.read())
            zip_file = zipfile.ZipFile(zip_stream_io, mode='r')

            # iterate all files in ZIP archive by the name.
            for filename in zip_file.namelist():
                # check file size.
                self._check_file_size(zip_file, filename)

                # open current iterated file from zip.
                file = zip_file.open(filename, mode='r')

                # check if file has '.csv' prefix.
                is_csv = self._check_if_csv(filename, file)

                if is_csv:
                    # if .csv -> read file and get generator with rows.
                    rows = self._read_file_and_get_generator(file, delimiter, encoding)

                    # get header of .csv file.
                    file_data = set_header(next(rows))  # FIXME - need to be refactored
                    header = file_data.header  # FIXME - need to be refactored
                    file_field = self._check_for_file_field(header, 'attachment')

                    for row in rows:
                        # check for attachment
                        if file_field is not None and row[file_field] != '' and row[file_field] in zip_file.namelist():
                            # try to save file from zip to local machine.
                            self._check_file_size(zip_file, row[file_field])
                            self._save_file(file_field, row, zip_file)
                        row_data = dict(zip(file_data.header, row))

                        # save this row to data.
                        file_data.append(row_data)

                    db_model_name = filename.split('.')[0]
                    # save parsed data to content with table name.
                    content[db_model_name] = file_data
                    logger.info(f"Extracted {filename} with size {file.tell()} bytes from ZIP.")

                file.close()

            zip_file.close()
            return content

        except Exception as exc:
            logger.warning(f"Something is wrong. CSV zip parse error - {exc}")
            raise ParseError('CSV parse error - %s' % str(exc))

    def _check_file_size(self, zip, filename):
        """Raises error if size of file greater than 100mb."""
        file_size = zip.getinfo(filename).file_size
        if file_size > 104857600:  # if file size is bigger than 100mb -> don't read.
            error_msg = f'File "{filename}" is too big. {int(file_size / (1024 ** 2))}mb > 100mb'

            raise ParseError(error_msg)

    def _check_if_csv(self, filename, file):
        """"""
        file_suffix = filename.split('.')[-1]
        if file_suffix == 'csv':
            return True
        else:
            try:
                decoded_file = file.read().decode('utf-8')
                hh = pycsv.Sniffer().has_header(decoded_file)  # has_header
                dd = pycsv.Sniffer().sniff(decoded_file)  # deduced_dialect
                logger.info(f"File {file.name} deduced as CSV file. Delimiter '{dd.delimiter}', strict '{dd.strict}'.")
                return True
            except Exception as e:
                logger.info(f"File {file.name} is not CSV file - {e}. Continue...")
                return False

    def _read_file_and_get_generator(self, file, delimiter, encoding):
        """"""
        binary = universal_newlines(file.read())
        rows = unicode_csv_reader(binary, delimiter=delimiter, charset=encoding)
        return rows

    def _save_file(self, file_field, row, zip):
        """"""
        # save file on server side and write file path to 'attachment' field.
        att_path = row[file_field]
        save_path = settings.MEDIA_ROOT
        complete_name = os.path.join(save_path, att_path)
        if not os.path.exists(complete_name):
            temp_folder = ''
            for folder in att_path.split('/')[:-1]:
                temp_folder += f"{folder}/"
                temp_path = os.path.join(save_path, temp_folder)
                if not os.path.exists(temp_path):
                    os.makedirs(temp_path)
                else:
                    logger.warning(f"Path {temp_path} is already exists.")

            # save file to local machine.
            with open(complete_name, "wb") as attachment:
                attachment.write(zip.read(att_path))
                file_size = zip.getinfo(att_path).file_size
                logger.info(f"Extracted {complete_name} file with size {file_size} bytes from ZIP.")
        else:
            logger.warning(f"File {complete_name} is already exists.")

    def _check_for_file_field(self, header, file_field):
        """"""
        if file_field in header:
            file_field_index = header.index(file_field)
            return file_field_index
        else:
            return None
