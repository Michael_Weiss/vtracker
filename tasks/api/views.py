from django.db.models import Q
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User as DjangoUser
from django.http import HttpResponseBadRequest
from django.conf import settings
from itertools import chain
import logging
import operator
import os

from rest_framework import generics, mixins, status
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema

from .parsers import ZIPCSVParser
from .renderers import CSVRenderer

from .serializers import (
    TaskSerialize,
    CommentSerialize,
    CommentBackupSerializer,
    ProfileSerialize,
    UserSerialize,
    CommentListSerialize,
    TaskBackupSerializer,
    TaskHistorySerializer,
    CommentHistorySerializer,
)
from tasks.models import Task, Comment
from accounts.models import Profile

from .permissions import (
    IsOwnerOrAssignee,
    CommentCreatePermission,
    TaskCommentsListPermission,
    ProfileEditPermission,
    PictureEditPermission,
    AttachmentEditPermission,
    UserEditPermission,
    CommentRUDPermission,
    CommentListPermission,
)

logger = logging.getLogger(__name__)


@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_description="description from swagger_auto_schema via method_decorator"
))
class TaskListCreateAPIView(mixins.CreateModelMixin, generics.ListAPIView):  # DetailView.
    """Get list of current user's tasks if request.GET or create new task if receive request.POST with valid data."""
    lookup_field = 'pk'
    serializer_class = TaskSerialize

    def get_queryset(self):
        qs = Task.objects.all()
        query = self.request.GET.get("q")
        if query is not None:
            qs = qs.filter(
                Q(title__icontains=query) |
                Q(description__icontains=query)
            ).distinct()
        return qs.filter(Q(user=self.request.user) | Q(assignee=self.request.user))

    def perform_create(self, serializers):
        serializers.save(user=self.request.user)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class TaskPostRudView(generics.RetrieveUpdateDestroyAPIView):  # DetailView.
    """Allows current user to see/edit/delete specific task by id if user has permissions."""
    lookup_field = 'pk'
    serializer_class = TaskSerialize
    permission_classes = [IsOwnerOrAssignee]

    def get_queryset(self):
        return Task.objects.all()


class TaskHistoryListAPIView(APIView):
    """Allows current user to see list of history records of specific task (and his comments)."""
    permission_classes = [IsOwnerOrAssignee]
    authentication_classes = [BasicAuthentication, SessionAuthentication]

    def get(self, request, *args, **kwargs):
        """Get sorted list with historical records (tasks and comments) by task pk."""
        try:
            obj1 = Task.objects.get(pk=kwargs['pk'])
        except Task.DoesNotExist as exc:
            return HttpResponseBadRequest(exc)  #FIXME: 400 or 404?
        serializer1 = TaskHistorySerializer(obj1)
        obj2 = Comment.objects.filter(task_id=kwargs['pk'])
        serializer2 = CommentHistorySerializer(obj2, many=True)
        ser_list = []
        for x in serializer2.data:
            for y in x['history']:
                ser_list.append(y)
        data = sorted(chain(serializer1.data['history'], ser_list), key=operator.itemgetter('history_date'))
        return Response(data)


class AttachmentUpdateAPIView(generics.UpdateAPIView):
    """Allows current user to see/edit/delete task attachment by task id if user is task owner."""
    serializer_class = [TaskSerialize]
    permission_classes = [AttachmentEditPermission]

    def get_queryset(self):
        return Profile.objects.all()

    def patch(self, request, *args, **kwargs):
        task_id = self.kwargs['pk']
        attachment = Task.objects.get(id=task_id)

        if attachment:
            if request.data.get('attachment') is not None:
                attachment.attachment = request.data.get('attachment')
                attachment.save()
            return Response(TaskSerialize(attachment).data, status=status.HTTP_200_OK)
        else:
            return Response({"message": "Image Not found"}, status=status.HTTP_400_BAD_REQUEST)


class CommentListAPIView(generics.ListAPIView):
    """Allows current user to see/create comments by task id if user is task owner or assignee."""
    serializer_class = CommentSerialize
    permission_classes = [CommentListPermission]

    def get_queryset(self):
        # this list should return a list of all comments
        # if currently authenticated user has rights.
        qs = Comment.objects.all()
        query = self.request.GET.get("q")  # filter by text.
        if query is not None:  # if filter is None --> skip.
            qs = qs.filter(
                Q(comment__icontains=query)
            ).distinct()
        return qs.filter(
            Q(commentator=self.request.user) | Q(task__user=self.request.user) | Q(task__assignee=self.request.user))


class CommentPostAPIView(generics.CreateAPIView):  # DetailView.
    """Allows current user to see/create comments by task id if user is task owner or assignee."""
    lookup_field = 'pk'
    serializer_class = CommentSerialize
    permission_classes = [CommentCreatePermission]

    def get_queryset(self):
        # this list should return a list of all comments
        # if currently authenticated user has rights.
        qs = Comment.objects.all()
        query = self.request.GET.get("q")  # filter by text.
        if query is not None:  # if filter is None --> skip.
            qs = qs.filter(
                Q(comment__icontains=query)
            ).distinct()
        return qs.filter(
            Q(commentator=self.request.user) | Q(task__user=self.request.user) | Q(task__assignee=self.request.user))

    def perform_create(self, serializers):
        serializers.save(commentator=self.request.user)

    def post(self, request, *args, **kwargs):
        try:
            task = Task.objects.get(pk=request.data['task'])  # 'kwargs.get('pk')' --> 'request.data['task']'
        except Task.DoesNotExist:
            return HttpResponseBadRequest("No task matches the given query.")
        if task.user == self.request.user or task.assignee == self.request.user:
            return self.create(request, *args, **kwargs)
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)


class CommentPostRudView(generics.RetrieveUpdateDestroyAPIView):
    """
    Allows current user to retrieve/edit/delete specific comment if request.user is comment owner.
    If request.user is not comment owner - only allows to retrieve comment.
    """
    lookup_field = 'pk'
    serializer_class = CommentSerialize
    permission_classes = [CommentRUDPermission]

    def get_queryset(self):
        return Comment.objects.all()


class TaskCommentsListView(generics.ListAPIView):
    """Allows current user to see list of task comments if request.user is task owner or assigned to it."""
    lookup_field = 'pk'
    serializer_class = CommentListSerialize
    permission_classes = [TaskCommentsListPermission]

    def get_queryset(self):
        return Comment.objects.filter(task_id=self.kwargs.get('pk'))


class ProfilePostRuView(generics.RetrieveUpdateAPIView):
    """
    Allows current user to edit 'Profile' fields if request.user is owner.
    If request.user is not owner of Profile and not anonymous  - allows to see Profile data.
    """
    serializer_class = ProfileSerialize
    permission_classes = [ProfileEditPermission]

    def get_queryset(self):
        pk = self.kwargs['pk']
        return Profile.objects.filter(user_id=pk)

    def get_object(self):
        """Returns the object the view is displaying."""
        queryset = self.filter_queryset(self.get_queryset())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        assert lookup_url_kwarg in self.kwargs, (
                'Expected view %s to be called with a URL keyword argument '
                'named "%s". Fix your URL conf, or set the `.lookup_field` '
                'attribute on the view correctly.' %
                (self.__class__.__name__, lookup_url_kwarg)
        )
        obj = get_object_or_404(queryset)

        # May raise a permission denied.
        self.check_object_permissions(self.request, obj)
        return obj


class AvatarUpdateAPIView(generics.UpdateAPIView):
    """
    Allows current user to edit 'Profile' picture field if request.user is owner.
    If request.user is not owner of Profile object and not anonymous  - allows to see picture.
    """
    serializer_class = ProfileSerialize
    permission_classes = [PictureEditPermission]

    def get_queryset(self):
        return Profile.objects.all()

    def patch(self, request, *args, **kwargs):
        picture_id = self.kwargs['pk']
        picture = Profile.objects.get(id=picture_id)

        if picture:
            if request.data.get('picture') is not None:
                picture.picture = request.data.get('picture')
                picture.save()
            return Response(ProfileSerialize(picture).data, status=status.HTTP_200_OK)
        else:
            return Response({"message": "Image Not found"}, status=status.HTTP_400_BAD_REQUEST)


class UserRuAPIView(generics.RetrieveUpdateAPIView):
    """
    Allows current user to edit 'User' model first_name and last_name fields if request.user is this 'User'.
    If request.user is not 'User' and not anonymous  - allows to see this fields.
    """
    lookup_field = 'pk'
    serializer_class = UserSerialize
    permission_classes = [UserEditPermission]
    queryset = DjangoUser.objects.all()


class BackupAPIView(APIView):
    """Export and import backup archives with tasks, comments and attachments."""
    authentication_classes = [BasicAuthentication, SessionAuthentication]
    renderer_classes = [CSVRenderer]  # this custom class create .zip file for 'Response' class.
    parser_classes = [ZIPCSVParser]

    def get(self, request):
        """Get .zip archive with .csv backup files."""
        data = {}
        # Append here your serialized data to 'data' dictionary when you want to back up it.
        tasks = Task.objects.filter(user=request.user)
        for task in tasks:
            if task.external_id is None:
                task.external_id = task.id
        data['tasks'] = TaskSerialize(tasks, many=True).data
        comments = Comment.objects.filter(commentator=request.user, task__user=request.user)
        for comment in comments:
            if comment.external_id is None:
                comment.external_id = comment.id
            if comment.ext_task_id is None and tasks.get(pk=comment.task_id).external_id is not None:
                comment.ext_task_id = tasks.get(pk=comment.task_id).external_id
            elif comment.ext_task_id is None and tasks.get(pk=comment.task_id).external_id is None:
                comment.ext_task_id = tasks.get(pk=comment.task_id).id
        data['comments'] = CommentBackupSerializer(comments, many=True).data
        logger.info(f"{len(comments)} comments % {len(tasks)} tasks to user '{request.user.username}'. Archiving...")
        return Response(data)

    def post(self, request, *args, **kwargs):
        """Parse .csv backup files and save it if all is OK."""
        resp_msgs = {'warn': []}
        if 'tasks' in request.data:
            new_tasks = []
            attch_to_add = []
            must_fields = ['assignee', 'attachment', 'creation_date', 'deadline', 'description', 'external_id', 'pk',
                           'status', 'title', 'user']
            if not all(elem in request.data['tasks'].header for elem in must_fields):
                return Response({"err": "Some fields in tasks.csv are missing."}, status=status.HTTP_400_BAD_REQUEST)
            for task in request.data['tasks']:
                # check if task['pk'] and task['external_id'] are an integer.
                try:
                    float(task['pk'])
                    float(task['external_id'])
                except ValueError:
                    return Response({"err": "Value of 'pk' or 'e_id' field in tasks.csv must be a number."},
                                    status=status.HTTP_400_BAD_REQUEST)

                # if task with same ID is not exists --> go to next check...
                if not Task.objects.filter(id=task['pk']).exists():
                    owner_id = task['user']
                    # if task with same external ID not exists --> add task to creation list.
                    if not Task.objects.filter(external_id=task['external_id']).exists() and self.has_perm(owner_id):
                        # delete empty attachment field.
                        if task['attachment'].strip(' ') == '':
                            del task['attachment']
                        # add attachment to 'to-add-list' and delete task field to prevent serializer error.
                        elif task['attachment'] != '':  # FIXED 2021-05-05
                            attch_to_add.append({'external_id': task['external_id'], 'attachment': task['attachment']})
                            del task['attachment']
                        new_tasks.append(task)
            serializer = TaskBackupSerializer(data=new_tasks, many=True)  # change: request.data['tasks'] --> new_tasks
            serializer.is_valid(raise_exception=True)
            serializer.save()

            # Add attachment path to the task if is any to add.
            save_path = settings.MEDIA_ROOT
            for task_info in attch_to_add:
                # next((item for item in serializer.data if item['user'] == task_info['attachment']), None)
                path = os.path.join(save_path, task_info['attachment'])
                if not os.path.exists(path):
                    # add warning message.
                    resp_msgs['warn'].append(f'Cannot find file path of "{task_info["attachment"]}" attachment.')
                task = Task.objects.get(external_id=task_info['external_id'])
                task.attachment = task_info['attachment']
                task.save()
            if 'comments' not in request.data:
                return Response(resp_msgs)

        if 'comments' in request.data:
            new_commnets = []
            must_fields = []
            if not all(elem in request.data['tasks'].header for elem in must_fields):
                return Response({"err": "Some fields in tasks.csv are missing."}, status=status.HTTP_400_BAD_REQUEST)
            for comment in request.data['comments']:
                owner_id = comment['commentator_id']
                # iterate parsed data for 'comments' and check if attached tasks are exist.
                if Task.objects.filter(id=comment['task_id']).exists() and self.has_perm(owner_id):
                    if not Comment.objects.filter(Q(id=comment['pk']) | Q(external_id=comment['external_id'])).exists():
                        # if task with this ID exists & comment is not --> add comment to creation list.
                        new_commnets.append(comment)
                elif Task.objects.filter(external_id=comment['task_id']).exists() and self.has_perm(owner_id):
                    if not Comment.objects.filter(Q(id=comment['pk']) | Q(external_id=comment['external_id'])).exists():
                        # if task with external_id exists --> update comment 'task_id' field and add to creation list.
                        db_task = Task.objects.get(external_id=comment['task_id'])
                        comment['task_id'] = db_task.pk
                        new_commnets.append(comment)
                elif Task.objects.filter(external_id=comment['ext_task_id']).exists() and self.has_perm(owner_id):
                    if not Comment.objects.filter(Q(id=comment['pk']) | Q(external_id=comment['external_id'])).exists():
                        # if task with external_id exists --> update comment 'task_id' field and add to creation list.
                        db_task = Task.objects.get(external_id=comment['ext_task_id'])
                        comment['task_id'] = db_task.pk
                        new_commnets.append(comment)
            serializer = CommentBackupSerializer(data=new_commnets, many=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response()
        resp_msgs['err'] = "Nothing was submitted or error during save comments. Check .zip parser code."
        return Response(resp_msgs,
                        status=status.HTTP_400_BAD_REQUEST)

    def has_perm(self, id_to_check):
        """"""
        if isinstance(id_to_check, str):
            try:
                id_to_check = int(id_to_check)
            except ValueError:
                return False
        if self.request.user.id != id_to_check:
            return False
        return True
