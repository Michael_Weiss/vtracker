from __future__ import unicode_literals

import zipfile
import logging
import unicodecsv as csv
from rest_framework.renderers import *
from six import BytesIO, text_type

logger = logging.getLogger(__name__)


class CSVRenderer(BaseRenderer):
    """
    Renderer which serializes to CSV. Return .zip file with .csv files and attachments.
    """

    media_type = 'application/zip'  # edit this when you want to edit output file extension.
    format = 'zip'  # edit this too when you want to edit output file extension.
    level_sep = '.'
    header = None
    labels = None  # {'<field>':'<label>'}
    writer_opts = None

    def render(self, data, media_type=None, renderer_context={}, writer_opts=None):
        """
        Renders serialized *data* into CSV. For a dictionary:
        """

        if data is None:
            return ''

        writer_opts = renderer_context.get('writer_opts', writer_opts or self.writer_opts or {})
        header = renderer_context.get('header', self.header)
        labels = renderer_context.get('labels', self.labels)
        encoding = renderer_context.get('encoding', settings.DEFAULT_CHARSET)

        csv_buffer = BytesIO()
        csv_writer = csv.writer(csv_buffer, encoding=encoding, **writer_opts)

        if isinstance(data, dict):
            # iterate models data and create corresponding .csv files and add this to .zip archive.
            in_memory = BytesIO()
            with zipfile.ZipFile(in_memory, mode='w') as zf:
                for model_data in data:
                    table = self.tablize(data[model_data], header=header, labels=labels)
                    for row in table:
                        if 'att_index' in locals() and row[att_index] is not None:
                            filepath = str(settings.MEDIA_ROOT.parent)+row[att_index]
                            # TODO: handle IsADirectoryError exception.
                            try:
                                with open(filepath, 'rb') as file:
                                    try:
                                        # exclude file name from file path such 'path/to/file.csv' --> 'file.csv'
                                        file_name = row[att_index].split('/')[-1]
                                        # TODO: if row[5] is task pk -> OK
                                        zf.write(filepath, f'{model_data}//{row[5]}//{file_name}')  # add file to .zip
                                        # TODO: if attachment is .csv file -> do it unrecognizable to parser.
                                        row[att_index] = f'{model_data}/{row[5]}/{file_name}'
                                        logger.info(f"Added file '{file_name}' to ZIP. Associated with '{model_data}'.")
                                    except Exception as exc:
                                        logger.warning(f"Invalid file name - {exc}")
                            except Exception as exc:
                                logger.warning(f"File opening error - {exc}")
                        if 'attachment' in row:
                            att_index = row.index('attachment')
                        csv_writer.writerow(row)
                    if 'att_index' in locals():
                        del att_index
                    zf.writestr(f'{model_data}.csv', csv_buffer.getvalue())
                    logger.info(f"Added {model_data} with size {len(csv_buffer.getvalue())} bytes to ZIP.")
                    csv_buffer.truncate(0)
                logger.info(f"Created achieve with size {len(in_memory.getvalue())} and {len(zf.filelist)} file(s).")
            return in_memory.getvalue()

        elif not isinstance(data, list):
            data = [data]
            table = self.tablize(data, header=header, labels=labels)
            for row in table:
                csv_writer.writerow(row)

        return csv_buffer.getvalue()

    def tablize(self, data, header=None, labels=None):
        """
        Convert a list of data into a table.

        If there is a header provided to tablize it will efficiently yield each
        row as needed. If no header is provided, tablize will need to process
        each row in the data in order to construct a complete header. Thus, if
        you have a lot of data and want to stream it, you should probably
        provide a header to the renderer (using the `header` attribute, or via
        the `renderer_context`).
        """
        # Try to pull the header off of the data, if it's not passed in as an
        # argument.
        if not header and hasattr(data, 'header'):
            header = data.header

        if data:
            # First, flatten the data (i.e., convert it to a list of
            # dictionaries that are each exactly one level deep).  The key for
            # each item designates the name of the column that the item will
            # fall into.
            data = self.flatten_data(data)

            # Get the set of all unique headers, and sort them (unless already provided).
            if not header:
                # We don't have to materialize the data generator unless we
                # have to build a header.
                data = tuple(data)
                header_fields = set()
                for item in data:
                    header_fields.update(list(item.keys()))
                header = sorted(header_fields)

            # Return your "table", with the headers as the first row.
            if labels:
                yield [labels.get(x, x) for x in header]
            else:
                yield header

            # Create a row for each dictionary, filling in columns for which the
            # item has no data with None values.
            for item in data:
                row = [item.get(key, None) for key in header]
                yield row

        elif header:
            # If there's no data but a header was supplied, yield the header.
            if labels:
                yield [labels.get(x, x) for x in header]
            else:
                yield header

        else:
            # Generator will yield nothing if there's no data and no header
            pass

    def flatten_data(self, data):
        """
        Convert the given data collection to a list of dictionaries that are
        each exactly one level deep. The key for each value in the dictionaries
        designates the name of the column that the value will fall into.
        """
        for item in data:
            flat_item = self.flatten_item(item)
            yield flat_item

    def flatten_item(self, item):
        if isinstance(item, list):
            flat_item = self.flatten_list(item)
        elif isinstance(item, dict):
            flat_item = self.flatten_dict(item)
        else:
            flat_item = {'': item}

        return flat_item

    def nest_flat_item(self, flat_item, prefix):
        """
        Given a "flat item" (a dictionary exactly one level deep), nest all of
        the column headers in a namespace designated by prefix.  For example:

         header... | with prefix... | becomes...
        -----------|----------------|----------------
         'lat'     | 'location'     | 'location.lat'
         ''        | '0'            | '0'
         'votes.1' | 'user'         | 'user.votes.1'

        """
        nested_item = {}
        for header, val in flat_item.items():
            nested_header = self.level_sep.join([prefix, header]) if header else prefix
            nested_item[nested_header] = val
        return nested_item

    def flatten_list(self, l):
        flat_list = {}
        for index, item in enumerate(l):
            index = text_type(index)
            flat_item = self.flatten_item(item)
            nested_item = self.nest_flat_item(flat_item, index)
            flat_list.update(nested_item)
        return flat_list

    def flatten_dict(self, d):
        flat_dict = {}
        for key, item in d.items():
            key = text_type(key)
            flat_item = self.flatten_item(item)
            nested_item = self.nest_flat_item(flat_item, key)
            flat_dict.update(nested_item)
        return flat_dict
