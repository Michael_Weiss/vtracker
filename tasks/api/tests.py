import datetime
import os
import pathlib

from django.contrib.auth.models import User
from django.urls import reverse
from django.conf import settings
from rest_framework.test import APITestCase
from datetime import date, timedelta
from PIL import Image
import zipfile
import io

from tasks.models import Task, Comment
from accounts.models import Profile
from .renderers import CSVRenderer


class TaskAPITestCase(APITestCase):
    def setUp(self):
        """Create user."""
        self.username = 'test'
        self.email = 'test@mail.com'
        self.password = 'test'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def generate_photo_file(self):
        """"""
        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(file, 'png')
        file.name = 'test_pic.png'
        file.seek(0)
        return file

    def test_create_task_API(self):
        """User create task via API."""
        self.client.login(username=self.username, password=self.password)
        url = reverse('task-api')
        data = {'title': 'test', 'assignee': self.user.pk}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)

        # check task.
        task = Task.objects.last()
        self.assertEqual(task.title, data.get('title'))
        self.assertEqual(task.assignee_id, data.get('assignee'))
        self.assertEqual(task.user_id, self.user.pk)

    def test_create_task_incorrect_title_API(self):
        """get 400 Bad Request"""
        self.client.login(username=self.username, password=self.password)
        url = reverse('task-api')
        bad_title = '1'  # too short. Must be >3 characters.
        data = {'title': bad_title, 'assignee': self.user.pk}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 400)  # Bad request 400

    def test_create_task_incorrect_deadline_API(self):
        """get 400 Bad Request"""
        self.client.login(username=self.username, password=self.password)
        url = reverse('task-api')
        bad_deadline = date.today() - timedelta(days=7)
        data = {'title': 'test', 'assignee': self.user.pk, 'deadline': bad_deadline.isoformat()}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 400)  # Bad request 400
        self.assertEqual(len(response.data), 1)
        self.assertIn('may not be', response.data['deadline'][0])

    def test_create_task_without_required_assignee_API(self):
        """"""
        self.client.login(username=self.username, password=self.password)
        url = reverse('task-api')
        data = {'title': 'test'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 400)  # Bad request 400

    def test_task_list_API(self):
        """"""
        self.client.login(username=self.username, password=self.password)
        url = reverse('task-api')

        # create two tasks.
        task1 = Task.objects.create(title='task1', assignee=self.user, user=self.user)
        task1.save()
        task2 = Task.objects.create(title='task2', assignee=self.user, user=self.user)
        task2.save()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['title'], task1.title)
        self.assertEqual(response.data[1]['title'], task2.title)


class AttachmentAPITestCase(APITestCase):
    def setUp(self):
        """Create user."""
        # create main user.
        self.username = 'test'
        self.email = 'test@mail.com'
        self.password = 'test'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)

        # create second user.
        self.hacker_usr = 'hacker'
        hacker_email = 'hacker@mail.com'
        self.hacker_pwd = 'hacker'
        self.hacker = User.objects.create_user(username=self.hacker_usr, email=hacker_email, password=self.hacker_pwd)
        self.assertEqual(self.hacker.username, self.hacker_usr)

        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        # create task.
        self.task = Task.objects.create(title='test', user_id=self.user.pk, assignee_id=self.user.pk)
        self.assertEqual(Task.objects.count(), 1)

    def generate_photo_file(self):
        """Generate image and return it."""
        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(file, 'png')
        file.name = 'test_pic.png'
        file.seek(0)
        return file

    def test_task_attachment_edit_API(self):
        """User update task attachment and get 200. Checks if task in upload folder."""
        self.client.login(username=self.username, password=self.password)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('task-attachment', kwargs={'pk': self.task.id})
        photo_file = self.generate_photo_file()
        data = {'attachment': photo_file}
        request = self.client.patch(url, data, format='multipart')
        edited_task = Task.objects.get(id=self.task.id)

        # check if all is ok.
        self.assertEqual(request.status_code, 200)
        err_msg = "Bad file name or save directory.\nCheck settings OR FileField's upload directory in 'models.py'."
        self.assertEqual(edited_task.attachment.name, 'uploads/' + photo_file.name, msg=err_msg)

        # delete test file.
        att_path = edited_task.attachment.path
        os.remove(att_path)

    def test_task_attachment_edit_disallowed_API(self):
        """"""
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('task-attachment', kwargs={'pk': self.task.id})
        photo_file = self.generate_photo_file()
        data = {'attachment': photo_file}
        request = self.client.patch(url, data, format='multipart')
        edited_task = Task.objects.get(id=self.task.id)

        # check if all is ok.
        self.assertEqual(request.status_code, 403)
        self.assertNotEqual(edited_task.attachment.name, 'uploads/' + photo_file.name)


class CommentAPITestCase(APITestCase):

    def setUp(self):
        """Create user."""
        self.username = 'test'
        self.email = 'test@mail.com'
        self.password = 'test'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)

        # create assignee user.
        assignee_usr = 'assignee'
        assignee_email = 'assignee@mail.com'
        self.assignee_pwd = 'assignee'
        self.assignee = User.objects.create_user(username=assignee_usr, email=assignee_email,
                                                 password=self.assignee_pwd)
        self.assertEqual(self.assignee.username, assignee_usr)

        # create another user.
        self.hacker_usr = 'hacker'
        hacker_email = 'hacker@mail.com'
        self.hacker_pwd = 'hacker'
        self.hacker = User.objects.create_user(username=self.hacker_usr, email=hacker_email, password=self.hacker_pwd)
        self.assertEqual(self.hacker.username, self.hacker_usr)

        # user log in.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        # create task.
        url = reverse('task-api')
        data = {'title': 'test', 'assignee': self.assignee.pk}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)
        self.task = Task.objects.last()

        # TODO: check task user and assignee

        # create comment.
        # url = reverse('comment-api-create', kwargs={'pk': self.task.pk})  # FIXME: delete this?
        url = reverse('comment-api-create')
        data = {'comment': 'test', 'task': self.task.pk}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)

        # check comment
        comment = Comment.objects.all()
        self.assertEqual(comment.count(), 1)

        self.comment = Comment.objects.last()

        # user log out.
        self.client.logout()

    def test_create_comment_assignee_API(self):
        """"""
        self.client.login(username=self.assignee.username, password=self.assignee_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.assignee.username, 'password': self.assignee_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-api-create')
        data = {'comment': 'test', 'task': self.task.pk}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)

        # check comment.
        comment = Comment.objects.last()
        self.assertEqual(comment.commentator_id, self.assignee.pk)
        self.assertEqual(comment.comment, data.get('comment'))
        self.assertEqual(comment.task.pk, self.task.pk)

    def test_create_comment_assignee_incorrect_task_API(self):
        """"""
        self.client.login(username=self.assignee.username, password=self.assignee_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.assignee.username, 'password': self.assignee_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        task_id = 404
        url = reverse('comment-api-create')
        data = {'comment': 'test', 'task': task_id}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Comment.objects.all().count(), 1)

    def test_create_comment_assignee_incorrect_text_API(self):
        """"""
        self.client.login(username=self.assignee.username, password=self.assignee_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.assignee.username, 'password': self.assignee_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        text = ''
        url = reverse('comment-api-create')
        data = {'comment': text, 'task': self.task.pk}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(Comment.objects.all().count(), 1)

    def test_create_comment_disallowed_task_API(self):
        """"""
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-api-create')
        data = {'comment': 'hacker_test', 'task': self.task.pk}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Comment.objects.all().count(), 1)

    def test_edit_comment_disallowed_task_API(self):
        """"""
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        data = {'comment': 'hacker_test'}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Comment.objects.all().count(), 1)

    def test_edit_comment_allowed_task_but_assignee_API(self):
        """"""
        self.client.login(username=self.assignee.username, password=self.assignee_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.assignee.username, 'password': self.assignee_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        text = 'test (edited)'
        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        data = {'comment': text}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

        # check comment
        self.assertEqual(Comment.objects.all().count(), 1)
        comment = Comment.objects.get(pk=self.comment.pk)
        self.assertNotEqual(comment.comment, text)

    def test_edit_comment_allowed_task_and_owner_API(self):
        """"""
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        text = 'test (edited)'
        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        data = {'comment': text}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 200)

        # check comment
        self.assertEqual(Comment.objects.all().count(), 1)
        comment = Comment.objects.get(pk=self.comment.pk)
        self.assertEqual(comment.comment, text)

    def test_show_comment_disallowed_task_API(self):
        """"""
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_show_comment_allowed_task_and_assignee_API(self):
        """"""
        self.client.login(username=self.assignee.username, password=self.assignee_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.assignee.username, 'password': self.assignee_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_show_comment_allowed_task_and_owner_API(self):
        """"""
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_delete_comment_disallowed_task_API(self):
        """"""
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Comment.objects.all().count(), 1)

    def test_delete_comment_allowed_task_and_assignee_API(self):
        """"""
        self.client.login(username=self.assignee.username, password=self.assignee_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.assignee.username, 'password': self.assignee_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Comment.objects.all().count(), 1)

    def test_delete_comment_allowed_task_and_owner_API(self):
        """"""
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-rud', kwargs={'pk': self.comment.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Comment.objects.all().count(), 0)

    def test_list_comments_hacker_API(self):
        """"""
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    def test_list_comments_assignee_API(self):
        """"""
        self.client.login(username=self.assignee.username, password=self.assignee_pwd)

        # obtain assignee JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.assignee.username, 'password': self.assignee_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_list_comments_owner_API(self):
        """"""
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('comment-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_list_task_comments_API(self):
        """"""
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('task-comments', kwargs={'pk': self.task.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)

    def test_task_list_comments_disallowed_API(self):
        """"""
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('task-comments', kwargs={'pk': self.task.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(len(response.data), 1)
        self.assertIn('do not have permission', response.data['detail'])

    def test_get_history_API(self):
        """"""
        # edit profile info.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        # change task.
        task = Task.objects.get(id=self.task.id)
        task.title = 'test (edited)'
        task.save()

        # get history via API.
        url = reverse('task-history-api', kwargs={'pk': self.task.pk})
        response = self.client.get(url)

        # check if status 200 and history has comments and tasks.
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[2]['title'], task.title)

    def test_get_history_bad_task_id_API(self):
        """"""
        # edit profile info.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        # try to get history of non-existing task via API.
        bad_task_pk = 404
        url = reverse('task-history-api', kwargs={'pk': bad_task_pk})
        response = self.client.get(url)

        # check if status 400 and error message is in content.
        self.assertEqual(response.status_code, 400)
        self.assertIn('Task matching query does not exist', response.content.decode("utf-8"))


class ProfileAPITestCase(APITestCase):
    """"""

    def setUp(self):
        """"""

        # create main user.
        self.username = 'test'
        self.email = 'test@mail.com'
        self.password = 'test'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)

        # create another user.
        self.hacker_usr = 'hacker'
        hacker_email = 'hacker@mail.com'
        self.hacker_pwd = 'hacker'
        self.hacker = User.objects.create_user(username=self.hacker_usr, email=hacker_email, password=self.hacker_pwd)
        self.assertEqual(self.hacker.username, self.hacker_usr)

        # add profile info.
        self.client.login(username=self.username, password=self.password)
        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        birthday = date.today() - timedelta(365)
        bio = 'test'
        level = 'Trainee'
        position = 'Engineer'
        first_name = 'John'
        last_name = 'Doe'
        url = reverse('profile-ru', kwargs={'pk': self.user.pk})
        path = str(pathlib.Path().absolute())

        # create image
        img = Image.new('RGB', (300, 300), color='white')
        img.save(str(settings.MEDIA_ROOT) + '/' + 'test.jpg')

        with open(path + '/cdn_test/media/test.jpg', 'rb') as picture:
            data = {'birth_date': birthday, 'bio': bio, 'level': level, 'position': position, 'picture': picture}
            response = self.client.put(url, data, format='multipart')
        self.assertEqual(response.status_code, 200)

        url = reverse('user-ru', kwargs={'pk': self.user.pk})
        data = {'first_name': first_name, 'last_name': last_name}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 200)

        # user log out
        self.client.logout()

    def test_edit_disallowed_profile(self):
        """"""
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        birthday = date.today() - timedelta(182)
        bio = 'error'
        level = 'Senior'
        position = 'other'
        first_name = 'Error'
        last_name = 'Error'
        url = reverse('profile-ru', kwargs={'pk': self.user.pk})
        data = {'birth_date': birthday, 'bio': bio, 'level': level, 'position': position}
        response = self.client.put(url, data, format='json')
        # TODO: add picture to profile
        self.assertEqual(response.status_code, 403)

        url = reverse('user-ru', kwargs={'pk': self.user.pk})
        data = {'first_name': first_name, 'last_name': last_name}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_edit_user_id_field_disallowed_API(self):
        """"""
        # edit profile info.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('profile-ru', kwargs={'pk': self.user.pk})
        data = {'user_id': self.hacker.pk}
        response = self.client.put(url, data, format='json')
        self.assertNotEqual(response.data['user'], data['user_id'])
        # TODO: add picture to profile
        profile = Profile.objects.get(user__username=self.username)
        self.assertNotEqual(profile.user_id, self.hacker.pk)

    def test_edit_picture_API(self):
        """"""
        # edit profile info.
        self.client.login(username=self.username, password=self.password)

        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.username, 'password': self.password}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('profile-ru', kwargs={'pk': self.user.pk})
        path = str(pathlib.Path().absolute())
        with open(path + '/cdn_test/media/error.jpg', 'rb') as picture:
            data = {'picture': picture}
            response = self.client.put(url, data, format='multipart')
            profile = Profile.objects.get(user__username=self.username)
            self.assertEqual(response.status_code, 200)
            self.assertIn('pictures/error', profile.picture.name)

    def test_edit_picture_disallowed_API(self):
        """"""
        # edit profile info.
        self.client.login(username=self.hacker_usr, password=self.hacker_pwd)
        # obtain JWT token.
        url = reverse('token-obtain-pair')
        data = {'username': self.hacker_usr, 'password': self.hacker_pwd}
        response = self.client.post(url, data, format='json')
        self.token = response.data['access']
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

        url = reverse('profile-ru', kwargs={'pk': self.user.pk})
        path = str(pathlib.Path().absolute())
        with open(path + '/cdn_test/media/error.jpg', 'rb') as picture:
            data = {'picture': picture}
            response = self.client.put(url, data, format='multipart')
            profile = Profile.objects.get(user__username=self.username)
            self.assertEqual(response.status_code, 403)
            self.assertIn('pictures/test', profile.picture.name)


class BackupAPITestCase(APITestCase):

    def setUp(self):
        """"""
        # create user.
        self.username = 'test'
        self.email = 'test@mail.com'
        self.password = 'test'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)
        # create second user.
        self.assignee_usr = 'assignee'
        self.assignee_email = 'assignee@mail.com'
        self.assignee_pwd = 'assignee'
        self.assignee = User.objects.create_user(username=self.assignee_usr,
                                                 email=self.assignee_email,
                                                 password=self.assignee_pwd)
        # create task with attachment.
        title = 'task'
        description = 'task description'
        status = 'to do'
        deadline = date.today() + timedelta(days=1)
        att_path = 'test.jpg'
        self.task = Task.objects.create(title=title,
                                        user=self.user,
                                        assignee=self.assignee,
                                        description=description,
                                        status=status,
                                        deadline=deadline,
                                        attachment=att_path
                                        )
        self.task.save()
        # check task.
        self.assertEqual(Task.objects.count(), 1)
        # create comment.
        message = 'test'
        self.comment = Comment.objects.create(comment=message, commentator=self.user, task=self.task)
        self.comment.save()
        # check comment.
        self.assertEqual(Comment.objects.count(), 1)

    def generate_photo_file(self):
        """"""
        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(file, 'png')
        file.name = 'test_pic.png'
        file.seek(0)
        return file

    def test_backup_get_API(self):
        """"""
        # create test file.
        if not os.path.exists(str(settings.MEDIA_ROOT) + '/' + 'test.jpg'):
            img = Image.new('RGB', (300, 300), color='white')
            img.save(str(settings.MEDIA_ROOT) + '/' + 'test.jpg')

        self.client.login(username=self.username, password=self.password)
        url = reverse('backup')
        request = self.client.get(url)
        self.assertTrue(request.is_rendered, msg="\nRender error. Check 'parsers.py' or 'views.py'.")
        self.assertEqual(request.status_code, 200, msg="\nWrong status code.")
        self.assertEqual(request.data['tasks'][0].__len__(), 10, msg="\nSome 'task' fields are missing or unnecessary.")
        stream = io.BytesIO(request.content)
        zf = zipfile.ZipFile(stream, mode='r')

        # check file names in .zip file
        self.assertEqual(zf.namelist().__len__(), 3, msg="\nSome files are missing or unnecessary. Check 'parsers.py'.")
        for name in ['tasks.csv', 'comments.csv', 'tasks/1/test.jpg']:
            self.assertTrue(name in zf.namelist())

        # check tasks.csv file.
        line_separator = b'\r\n'
        csv_lines = zf.read('tasks.csv').split(line_separator)
        delimiter = b','
        for line in [0, 1]:
            self.assertEqual(csv_lines[line].split(delimiter).__len__(), 10)

        # check comments.csv file
        line_separator = b'\r\n'
        csv_lines = zf.read('comments.csv').strip(b'\x00').split(line_separator)
        delimiter = b','
        for line in [0, 1]:
            self.assertEqual(csv_lines[line].split(delimiter).__len__(), 6, msg="\nIssue with fields.")

    def test_backup_post_API(self):
        """"""
        # create test file.
        img = Image.new('RGB', (300, 300), color='white')
        img.save(str(settings.MEDIA_ROOT) + '/' + 'test.jpg')

        # get .zip backup file from the server.
        self.client.login(username=self.username, password=self.password)
        url = reverse('backup')
        request = self.client.get(url)
        self.assertTrue(request.is_rendered, msg="\nRender error. Check 'parsers.py' or 'views.py'.")
        self.assertEqual(request.status_code, 200, msg="\nWrong status code.")
        self.assertEqual(request.data['tasks'][0].__len__(), 10, msg="\nSome 'task' fields are missing or unnecessary.")
        stream = io.BytesIO(request.content)

        # after we've received .zip file --> delete old task.
        self.assertEqual(Task.objects.count(), 1)
        old_task = Task.objects.last()  # save deleted task to compare with restored.
        self.task.delete()
        self.assertEqual(Task.objects.count(), 0)

        # send .zip file via POST to the server.
        request = self.client.post(url, stream.getvalue(), content_type='application/zip')
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.accepted_media_type, 'application/zip')
        self.assertEqual(Task.objects.count(), 1)
        new_task = Task.objects.last()

        # compare deleted and restored tasks with each other by attribute value.
        for attribute in ['deadline', 'assignee', 'user', 'description', 'creation_date', 'status']:
            self.assertEqual(getattr(new_task, attribute), getattr(old_task, attribute))

        # delete test file.
        os.remove(new_task.attachment.path)



    def test_backup_post_incorrect_data_API(self):
        """Send backup with incorrect data (all fields are incorrect) and get 400 Bad Request."""
        self.client.login(username=self.username, password=self.password)

        # create dictionary with incorrect task data.
        task = {}
        t_keys = ['pk', 'user', 'assignee', 'title', 'description', 'status', 'creation_date', 'deadline',
                  'external_id', 'attachment']
        t_values = [404, 404, 404, '', '', 'error', datetime.date(3021, 1, 1), datetime.date(1970, 1, 1), 404, '']
        for key, value in zip(t_keys, t_values):
            task[key] = value

        # create dictionary with incorrect comment data.
        comment = {}
        c_keys = ['pk', 'task_id', 'comment', 'commentator_id', 'external_id', 'ext_task_id']
        c_values = [404, 404, '', 404, 404, 404]
        for key, value in zip(c_keys, c_values):
            comment[key] = value

        # convert incorrect data to .csv files inside .zip archive.
        data = {'tasks': [task], 'comments': [comment]}
        renderer = CSVRenderer()
        result = renderer.render(data=data)

        # try to send bad .zip file via API.
        url = reverse('backup')
        request = self.client.post(url, result, content_type='application/zip')
        self.assertEqual(request.status_code, 200, msg='\nRequest must be "200". Check your code.')
        self.assertEqual(Task.objects.count(), 1)
        self.assertEqual(Comment.objects.count(), 1)

    def test_backup_post_disallowed_data_API(self):
        """Send backup with incorrect data (all fields are incorrect) and get 400 Bad Request."""
        self.client.login(username=self.username, password=self.password)

        # create new user.
        username = 'foo'
        email = 'foo@foo.foo'
        password = 'foo'
        new_user = User.objects.create_user(username=username, email=email, password=password)
        self.assertEqual(User.objects.last(), new_user)

        self.assertEqual(Task.objects.count(), 1)

        # create dictionary with incorrect task data.
        task = {}
        t_keys = ['pk', 'user', 'assignee', 'title', 'description', 'status', 'creation_date', 'deadline',
                  'external_id', 'attachment']
        future = datetime.date.today() + timedelta(365)
        past = datetime.date.today() - timedelta(365)
        t_values = [2, 1, 2, 'test', 'test', 'to do', past, future, 2, '']
        for key, value in zip(t_keys, t_values):
            task[key] = value

        # create dictionary with incorrect comment data.
        for key, value in zip(t_keys, t_values):
            task[key] = value

        # create dictionary with correct comment data.
        comment = {}
        c_keys = ['pk', 'task_id', 'comment', 'commentator_id', 'external_id', 'ext_task_id']
        c_values = [2, 2, 'test', 1, 2, 2]
        for key, value in zip(c_keys, c_values):
            comment[key] = value

        # convert incorrect data to .csv files inside .zip archive.
        data = {'tasks': [task], 'comments': [comment]}
        renderer = CSVRenderer()

        # try to send .csv data without each existing field.
        url = reverse('backup')

        t_bad_keys = ['user']
        bad_id = new_user.id

        for key in t_bad_keys:
            data = {'tasks': [task], 'comments': [comment]}

            n_task = task.copy()  # duplicate correct 'task' list.
            del n_task[key]  # FIXME
            n_task[key] = bad_id
            data['tasks'] = [n_task]

            n_comment = comment.copy()  # duplicate correct 'task' list.
            del n_comment['commentator_id']  # FIXME
            n_comment['commentator_id'] = bad_id
            data['comments'] = [n_comment]

            result = renderer.render(data=data)  # convert to .zip file
            request = self.client.post(url, result, content_type='application/zip')
            self.assertEqual(request.status_code, 200, msg='\nRequest must be "200". Check your code.')
            self.assertEqual(Task.objects.count(), 1)
            self.assertEqual(Comment.objects.count(), 1)

        c_bad_keys = ['commentator_id']

        for key in c_bad_keys:
            data = {'tasks': [task], 'comments': [comment]}

            n_comment = comment.copy()  # duplicate correct 'task' list.
            del n_comment[key]  # FIXME
            n_comment[key] = bad_id
            data['comments'] = [n_comment]
            result = renderer.render(data=data)  # convert to .zip file
            request = self.client.post(url, result, content_type='application/zip')
            # self.assertEqual(request.status_code, 400, msg='\nRequest must be "400 Bad Request". Check your code.')
            self.assertEqual(Task.objects.count(), 2)
            self.assertEqual(Comment.objects.count(), 1)

    def test_backup_post_without_fields_API(self):
        """Send backup without required field and get 400 Bad Request."""
        self.client.login(username=self.username, password=self.password)

        # create dictionary with correct task data.
        task = {}
        t_keys = ['pk', 'user', 'assignee', 'title', 'description', 'status', 'creation_date', 'deadline',
                  'external_id', 'attachment']
        future = datetime.date.today() + timedelta(365)
        past = datetime.date.today() - timedelta(365)
        t_values = [2, 1, 2, 'test', 'test', 'to do', past, future, 2, '']
        for key, value in zip(t_keys, t_values):
            task[key] = value

        # create dictionary with correct comment data.
        comment = {}
        c_keys = ['pk', 'task_id', 'comment', 'commentator_id', 'external_id', 'ext_task_id']
        c_values = [2, 2, 'test', 1, 2, 2]
        for key, value in zip(c_keys, c_values):
            comment[key] = value

        # convert correct data to .csv files inside .zip archive.
        data = {'tasks': [task], 'comments': [comment]}
        renderer = CSVRenderer()

        # try to send .csv data without each existing field.
        url = reverse('backup')
        for key in t_keys:
            n_task = task.copy()  # duplicate correct 'task' list.
            del n_task[key]
            data['tasks'] = [n_task]
            result = renderer.render(data=data)  # convert to .zip file
            request = self.client.post(url, result, content_type='application/zip')
            self.assertEqual(request.status_code, 400, msg='\nRequest must be "400 Bad Request". Check your code.')

    def test_backup_post_incorrect_field_API(self):
        """Send backup with incorrect field and get 400 Bad Request."""
        self.client.login(username=self.username, password=self.password)

        # create dictionary with correct task data.
        task = {}
        t_keys = ['pk', 'user', 'assignee', 'title', 'description', 'status', 'creation_date', 'deadline',
                  'external_id', 'attachment']
        future = datetime.date.today() + timedelta(365)
        past = datetime.date.today() - timedelta(365)
        t_values = [2, 1, 2, 'test', 'test', 'to do', past, future, 2, '']
        for key, value in zip(t_keys, t_values):
            task[key] = value

        # create dictionary with correct comment data.
        comment = {}
        c_keys = ['pk', 'task_id', 'comment', 'commentator_id', 'external_id', 'ext_task_id']
        c_values = [2, 2, 'test', 1, 2, 2]
        for key, value in zip(c_keys, c_values):
            comment[key] = value
        data = {'tasks': [task], 'comments': [comment]}

        # try to send .csv data without fields.
        url = reverse('backup')
        renderer = CSVRenderer()
        incorrect_keys = ['pk', 'user', 'assignee', 'title', 'status', 'deadline', 'external_id', 'attachment']
        incorrect_val = ['error', 404, 404, '', 'error', datetime.date(1970, 1, 1), 'error', 'error/err.err']
        for key, value in zip(incorrect_keys, incorrect_val):
            n_task = task.copy()  # duplicate correct 'task' list.
            n_task[key] = value
            data['tasks'] = [n_task]
            result = renderer.render(data=data)  # convert to .zip file
            request = self.client.post(url, result, content_type='application/zip')
            if key == 'attachment':
                self.assertEqual(request.data, None)
            elif key in ['user']:
                self.assertEqual(request.status_code, 200, msg='\nRequest must be "200". Check your code.')
                self.assertEqual(Task.objects.count(), 1)
                self.assertEqual(Comment.objects.count(), 1)
            else:
                self.assertEqual(request.status_code, 400, msg='\nRequest must be "400 Bad Request". Check your code.')
