import datetime

from rest_framework import serializers
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords
from django.conf import settings
from datetime import date

from tasks.models import Task, Comment
from accounts.models import Profile


class TaskSerialize(serializers.ModelSerializer):  # forms.ModelForm.
    class Meta:
        model = Task
        fields = [
            'pk',
            'user',
            'assignee',
            'title',
            'description',
            'status',
            'creation_date',
            'deadline',
            'attachment',
            'external_id',
        ]
        read_only_fields = ['pk', 'user', 'creation_date']
        extra_kwargs = {'assignee': {'required': True},
                        'user': {'required': True},
                        'attachment': {'allow_empty_file': True, 'required': False}}

    # this is how you can validate fields:
    # def validate_<fieldname>():

    def validate_title(self, value):
        qs = Task.objects.filter(title__iexact=value)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)  # exclude current instance values.
        if qs.exists():
            raise serializers.ValidationError("The title must be unique.")
        if len(value) < 4:  # set minimal number of characters here.
            raise serializers.ValidationError("This is not long enough.")
        return value

    def validate_deadline(self, value):
        if value is not None:
            if value < date.today():
                raise serializers.ValidationError('Deadline date may not be lower than current date.')
            return value


class HistoricalRecordField(serializers.ListField):
    child = serializers.DictField()

    def to_representation(self, data):
        return super().to_representation(data.values())


class TaskHistorySerializer(serializers.ModelSerializer):
    """"""
    history = HistoricalRecordField(read_only=True)

    class Meta:
        model = Task
        fields = ['history']


class CommentHistorySerializer(serializers.ModelSerializer):
    """"""
    history = HistoricalRecordField(read_only=True)

    class Meta:
        model = Comment
        fields = ['history']


class TaskBackupSerializer(serializers.ModelSerializer):  # forms.ModelForm.
    # user = serializers.IntegerField()
    # assignee = serializers.IntegerField()
    # attachment = serializers.FilePathField(settings.MEDIA_ROOT)

    class Meta:
        model = Task
        fields = [
            'pk',
            'user',
            'assignee',
            'title',
            'description',
            'status',
            'creation_date',
            'deadline',
            'attachment',
            'external_id',
        ]
        read_only_fields = ['pk', 'creation_date']
        extra_kwargs = {'assignee': {'required': True},
                        'user': {'required': True},
                        'attachment': {'allow_empty_file': True, 'required': False}}

    # this is how you can validate fields:
    # def validate_<fieldname>():

    def validate_title(self, value):
        qs = Task.objects.filter(title__iexact=value)
        if self.instance:
            qs = qs.exclude(pk=self.instance.pk)  # exclude current instance values.
        if qs.exists():
            raise serializers.ValidationError("The title must be unique.")
        if len(value) < 4:  # set minimal number of characters here.
            raise serializers.ValidationError("This is not long enough.")
        return value

    def validate_user(self, value):
        """"""
        db_user = User.objects.filter(id=value.pk)  # 'value.pk' instead of 'value'
        if db_user.exists():
            return value
        else:
            raise serializers.ValidationError(f'User with pk {value} does not exist.')

    def validate_assignee(self, value):
        """"""
        db_assignee = User.objects.filter(id=value.pk)  # 'value.pk' instead of 'value'
        if db_assignee.exists():
            return value
        else:
            raise serializers.ValidationError(f'Assignee with pk {value} does not exist.')

    def validate_creation_date(self, value):
        """"""
        if value != '' and isinstance(value, datetime.date) and value > datetime.date.today():
            raise serializers.ValidationError(f'Creation date cannot be in the future.')
        else:
            return value

    def validate_deadline(self, value):
        """"""
        if value != '' and isinstance(value, datetime.date) and value < datetime.date.today():
            raise serializers.ValidationError(f'Deadline cannot be in the past.')
        else:
            return value


class CommentSerialize(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = [
            'pk',
            'task',
            'comment',
            'commentator',
        ]
        read_only_fields = ['pk', 'commentator']

    def validate_comment(self, value):
        if len(value) < 1:  # set minimal number of characters here.
            raise serializers.ValidationError("Comment must contain at least 1 character.")
        return value


class CommentBackupSerializer(serializers.ModelSerializer):
    # FIXME: task_id and commentator_id are 'readOnlyField'. Why???
    task_id = serializers.IntegerField()
    commentator_id = serializers.IntegerField()

    class Meta:
        model = Comment
        fields = [
            'pk',
            'task_id',
            'comment',
            'commentator_id',
            'external_id',
            'ext_task_id'
        ]
        extra_kwargs = {'ext_task_id': {'required': False}}

    def validate(self, data):
        """"""
        try:
            task = Task.objects.get(pk=data['task_id'])
            if data['commentator_id'] in [task.assignee_id, task.user_id]:
                return data
            else:
                raise serializers.ValidationError('Commentator must be assignee or task owner.')
        except Task.DoesNotExist:
            raise serializers.ValidationError('Task with PK does not exist.')


class CommentListSerialize(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = [
            'pk',
            'task',
            'comment',
            'commentator',
        ]
        read_only_fields = ['pk', 'task', 'comment', 'commentator']


class ProfileSerialize(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = [
            'birth_date',
            'bio',
            'picture',
            'level',
            'position',
            'user',
        ]
        read_only_fields = ['user']


class UserSerialize(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
        ]
