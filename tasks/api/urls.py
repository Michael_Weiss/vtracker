
"""vTasker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# Django frameworks.
from django.urls import path
# Installed frameworks.
from rest_framework_simplejwt import views as jwt_views
# Project import.
# ...

from .views import (
    TaskPostRudView,
    TaskCommentsListView,
    TaskListCreateAPIView,
    TaskHistoryListAPIView,
    CommentPostRudView,
    CommentPostAPIView,
    CommentListAPIView,
    AvatarUpdateAPIView,
    AttachmentUpdateAPIView,
    ProfilePostRuView,
    BackupAPIView,
    UserRuAPIView,
)

urlpatterns = [
    path('task/<int:pk>/attachment/', AttachmentUpdateAPIView.as_view(), name='task-attachment'),
    path('task/<int:pk>/comments/', TaskCommentsListView.as_view(), name='task-comments'),
    path('task/<int:pk>/history/', TaskHistoryListAPIView.as_view(), name='task-history-api'),
    path('task/<int:pk>/', TaskPostRudView.as_view(), name='task-rud'),
    path('task/', TaskListCreateAPIView.as_view(), name='task-api'),
    path('comment/create/', CommentPostAPIView.as_view(), name='comment-api-create'),  # 'comment/<int:pk>/create/' --> 'comment/create/'
    path('comment/<int:pk>/', CommentPostRudView.as_view(), name='comment-rud'),
    path('comment/', CommentListAPIView.as_view(), name='comment-list'),
    path('user/<int:pk>/', UserRuAPIView.as_view(), name='user-ru'),
    path('profile/<int:pk>/picture/', AvatarUpdateAPIView.as_view(), name='avatar-update-api'),
    path('profile/<int:pk>/', ProfilePostRuView.as_view(), name='profile-ru'),
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token-refresh'),
    path('token/verify/', jwt_views.TokenVerifyView.as_view(), name='token_verify'),
    path('backup/', BackupAPIView.as_view(), name='backup'),
]
