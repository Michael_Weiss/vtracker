"""vtasker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# Django's frameworks
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, re_path, include
from django.contrib.auth.decorators import login_required
# Installed frameworks
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
# Project import
from tasks.views import TaskList, sandbox_view


schema_view = get_schema_view(
   openapi.Info(
      title="vTasker API",
      default_version='v1',
      description="You can use this API to handle data from vTasker database in your apps. For this you need to be registed and authenticated.",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="mb@freikom.de"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path('', login_required(TaskList.as_view(), login_url='/accounts/login/'), name='task-all'),
    path('admin/', admin.site.urls),
    path('api/', include('tasks.api.urls')),
    path('api/', include('chat.api.urls')),
    path('api/', include('accounts.api.urls')),
    path('task/', include('tasks.urls')),
    path('accounts/', include('accounts.urls')),
    path('chat/', include('chat.urls')),
    path('game/', include('game.urls')),
    path('sandbox_test/', sandbox_view, name='sandbox_test'),

    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)