"""
ASGI config for vtasker project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application
from channels.auth import AuthMiddlewareStack
import chat.routing
import game.routing

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'vtasker.settings')

application = ProtocolTypeRouter({
    'http': get_asgi_application(),
    'websocket': AuthMiddlewareStack(
        URLRouter(
            # chat connection.
            chat.routing.websocket_urlpatterns +
            # game connection.
            game.routing.websocket_urlpatterns)
    )
})
