# Django task tracker

This web application allows users to register and create tasks and then filter them on the home page.

## Install

Create a virtual environment in the repository folder:

    python3 -m venv /path/to/repo/folder/

In my case, the path to the repository: `/Users/mishabeliy/code/vtasker/`

The following folders should appear in the repository folder: `bin` , `include` and `lib` .
Now you need to activate the virtual environment. In the directory where you installed the virtual environment, use the following command:

    source bin/activate

You can check the path to the python with the following command:

    which python

If the path leads to the repository folder where you installed the virtual environment - then you can move on to the next point.
Install all the necessary packages using this command:

    pip install -r requirements.txt

If you don't have the latest version of Ubuntu (18.04 or later) - some packages may be incompatible.
Worked on Ubuntu 20.04.1.
Python 3.8.5

## To-Do
Check the python version:

    python -V

If python version 3 or higher, use this commands:

    python manage.py makemigrations
    python manage.py migrate
    python manage.py runserver

Then start a Redis server on port 6379 with the following command:

    docker run -p 6379:6379 -d redis:5

If you don't have Docker installed, click on the following [link](https://docs.docker.com/engine/install/).

**Important**: You must be in the repository directory

**Now** go to your browser and go to the address:

    127.0.0.1:8000