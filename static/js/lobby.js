import {ajaxSetup} from './login.js';

let createButton = $('#chat-create-button');
let chatCreateInput = $('#chat-create-input')

function createChat(name){
    $.post(`/api/chat/`, {
        name: name,
    })
        .done(function() {
            window.location.replace(`/chat/${name}`);
        })
        .fail(function () {
            console.log(arguments[0].responseJSON["detail"]);
            ajaxSetup().then(function (){
                console.log('Trying to create chat again...')
                createChat(name);
            }).catch(function (){
                console.log('ajax Setup failed.')
            })
        });
}

function drawChatCard(chat){
    const chatItem = `
        <a href="/chat/${chat.name}/" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">${chat.name}</h5>
                <small>${chat.users.length} user(s)</small>
            </div>
            <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
            <small>Creator: ${chat.creator}</small>
        </a>`;
    $(chatItem).appendTo('#chat-list');
}

function getChatList(){
        $.getJSON(`/api/chats/`, function (data) {
        for (let i = 0; i < data.length; i++) {
            drawChatCard(data[i]);
        }
    });
}

$( document ).ready(function() {
    console.log( "Document " + "lobby.js" + " is ready!" );
    ajaxSetup().then(function (){
        console.log('Trying to get chat list...')
        getChatList();
    }).catch(function (){
        console.log('ajax Setup failed.')
    })


    createButton.click(function () {
        if (chatCreateInput.val().length > 0) {
            createChat(chatCreateInput.val());
        }
    });
});
