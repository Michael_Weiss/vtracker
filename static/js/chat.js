import {ajaxSetup, checkOrRefreshToken} from './login.js';

// TODO: What must this script do ?
// retrieve chat users
// update users (if someone was deleted - show that)
// retrieve last n-number of messages
// send message
// handle .onmessage and get message via API by retrieved ID
// search users by the name
// --------------------------------------------------------
// ref: https://github.com/narrowfail/django-channels-chat/
// ref: https://github.com/zhukov/webogram/

// TODO: If user has no pictures --> standard picture.

let chatButton = $('#chat-btn');
let chatInput = $('#chat-input');
let messageList = $('#messages');
let inviteButton = $('#chat-invite-button');
let deleteButton = $('#delete-button');
let leaveButton = $('#leave-button');
const csrftoken = Cookies.get('csrftoken');

$.ajaxSetup({
    headers: {
        'X-CSRFToken': csrftoken
    }
});

function updateUserList(chat_id){
    $.getJSON(`/api/chat/${chat_id}/users/`, function (data) {
        for (let i = 0; i < data.length; i++) {
            const userItem = `
                    <a class="list-group-item user" target="_blank" href="/accounts/profile/${data[i]['user']}/">
                        <div class="profile-image float-md-right">
                            <img src="${data[i].picture}" id="pic-${data[i]['user']}" class="rounded-circle" style="width:70px;height:auto;">    
                        </div>
                        ${data[i]['user']}
                    </a>`;
            $(userItem).appendTo('#user-list');
        }
    });
}

function drawMessage(message){
    const date = new Date(message.timestamp);
    const pic_link = document.getElementById(`pic-${message.sender}`).src;
    const messageItem = `
            <li class="list-group-item">
                <div class="row">
                    <div class="col-lg-1 col-md-2 col-3">
                        <div class="profile-image float-md-right">
                            <img src="${pic_link}" class="rounded-circle" style="width:70px;height:auto;">    
                        </div>
                    </div>
                    <div class="col-lg col-md col">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="m-t-0 m-b-0">${message.sender}</h5>
                            <small>${date.toLocaleString("en-US")}</small>
                        </div>
                        <p class="mb-1">${message.message}</p>
                    </div>
                </div>
                
            </li>`;
    $(messageItem).appendTo('#messages');
}

function getConversation(chat_id){
    $.getJSON(`/api/chat/${chat_id}/messages/`, function (data) {
        messageList.children('.message').remove();
        let count = data.length - 1;
        for (let i = 0; i <= count; i++) {
            drawMessage(data[i]);
        }
        messageList.animate({scrollTop: messageList.prop('scrollHeight')});
    });
}

function getMessageByID(message){
    let id = JSON.parse(message).message
    $.getJSON(`/api/message/${id}/`, function (data) {
        if (data.recipient === currentChat ||
            (data.recipient === currentChat && data.sender === currentUser)) {
            drawMessage(data);
        }
        messageList.animate({scrollTop: messageList.prop('scrollHeight')});
    }).fail(function () {
        console.log(arguments[0].responseJSON["detail"]);
        ajaxSetup().then(function (){
            console.log('Trying to get message again...')
            getMessageByID(message);
        }).catch(function (){
            console.log('ajax Setup failed.')
        })
    });
}

function sendMessage(chat, message){
    $.post(`/api/message/`, {
        recipient: chat,
        message: message
    }).fail(function () {
        console.log(arguments[0].responseJSON["detail"]);
        ajaxSetup().then(function (){
            console.log('Trying to send message again...')
            sendMessage(chat, message);
        }).catch(function (){
            console.log('ajax Setup failed.')
        })
    });
}

function deleteChat(chatId){
    $.ajax({
        url: `/api/chat/${chatId}/delete/`,   // search URL (API)
        type: "DELETE",       // Querying means getting in HTTP terms
        success: function (){
            window.location.replace(`/chat/select/`);
        }, // Handle success scenario here.
        error: function () {
            console.log(arguments[0].responseJSON["detail"]);
            ajaxSetup().then(function (){
                console.log('Trying to delete chat again...')
                deleteChat(chatId);
            }).catch(function (){
                console.log('ajax Setup failed.')
            })

        } // Handle server-side errors here.
    });
}

function leaveChat(chatId){
    let data = {'self': 'True'};
    $.ajax({
        url: `/api/chat/${chatId}/leave/`,   // search URL (API)
        data : JSON.stringify(data),
        type : 'PATCH',
        contentType : 'application/json',
        processData: false,
        dataType: 'json',
        success: function (){
            window.location.replace(`/chat/select/`);
        }, // Handle success scenario here.
        error: function () {
            console.log(arguments[0].responseJSON["detail"]);
            ajaxSetup().then(function (){
                console.log('Trying to delete chat again...')
                leaveChat(chatId);
            }).catch(function (){
                console.log('ajax Setup failed.')
            })
        } // Handle server-side errors here.
    });

}

// TODO: admin can ban and kick users from the chat.
// function removeUser(chatId){}

$(document).ready(function () {
    ajaxSetup().then(function (){
        console.log('Trying to get user list and chat messages...')
        updateUserList(currentChat);
        getConversation(currentChat);
    }).catch(function (){
        console.log('ajax Setup failed.')
    })

    const roomID = JSON.parse(document.getElementById('room-id').textContent);

    // create new Web Socket channel to communicate with server
    // TODO: don't connect to WebSocket if user has invalid token.
    // TODO: disconnect from WebSocket if user has invalid token.
    const socket = new WebSocket(
        'ws://'
        + window.location.host
        + '/ws/chat/'
        + roomID
        + '/?session_key=${sessionKey}'
    );

    chatInput.keypress(function (e) {
        if (e.keyCode === 13)
            chatButton.click();
    });

    chatButton.click(function () {
        if (chatInput.val().length > 0) {
            sendMessage(currentChat, chatInput.val());
            chatInput.val('');
        }
    });

    inviteButton.click(function () {
        window.location.replace(`/chat/${roomID}/invite/`);
    });

    deleteButton.click(function () {
        deleteChat(currentChat)
    });

    leaveButton.click(function () {
        leaveChat(currentChat)
    });

    socket.onmessage = function (e) {
        // FIXME: (FIXED!) didn't work! New messages are not going trough!
        getMessageByID(e.data);
        getMessageByID(function repeat(result){})
    };
});

