import io
import os
from datetime import date, timedelta

from PIL import Image
from django.conf import settings
from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

from .models import Profile


class RegisterTestCase(TestCase):

    def setUp(self):
        """
        Initial create of user instance.
        """
        self.initUsr = 'test'
        self.initEmail = 'test@mail.com'
        self.initPwd = 'test'
        self.user = User.objects.create_user(username=self.initUsr, email=self.initEmail, password=self.initPwd)
        self.user.save()

    def test_user_was_created(self):
        """
        Testing if test user is created.
        """
        self.assertEqual(self.user.username, 'test')
        self.assertEqual(self.user.email, 'test@mail.com')

    def test_user_register(self):
        """
        Testing if user can be created via POST method.
        """
        username = 'dummy'
        email = 'dummy@user.com'

        # request with correct and complete user data to sign up.
        response = self.client.post(
            '/accounts/register/',
            {'username': username, 'email': email, 'password1': 'dummy', 'password2': 'dummy'},
        )
        dummy_user = User.objects.get(username=username)

        # comparisons and checks
        self.assertEqual(response.status_code, 302)
        self.assertIsNotNone(dummy_user)
        self.assertTrue(dummy_user.is_active)
        self.assertTrue(dummy_user.is_authenticated)
        self.assertFalse(dummy_user.is_superuser)
        self.assertFalse(dummy_user.is_staff)
        self.assertFalse(dummy_user.is_anonymous)

    def test_user_register_when_already_exist(self):
        """
        Error if username or email already exists.
        """

        # request with already existing user data to sign up.
        response = self.client.post(
            '/accounts/register/',
            {'username': self.initUsr, 'email': self.initEmail, 'password1': self.initPwd, 'password2': self.initPwd},
        )

        # comparisons and checks.
        for StatusCode in [302, 301, 404, 500]:
            self.assertNotEqual(response.status_code, StatusCode)
        self.assertIn('invalid username', str(response.content))

    def test_user_register_blacklisted_name(self):
        """
        Error if username in black list.
        """

        # request with already existing user data to sign up.
        bad_name = 'nigger'
        email = 'dummy@user.com'
        password = 'nigger'
        response = self.client.post(
            '/accounts/register/',
            {'username': bad_name, 'email': email, 'password1': password, 'password2': password},
        )

        # comparisons and checks.
        for st_code in [302, 301, 403, 500]:
            self.assertNotEqual(response.status_code, st_code)
        self.assertEqual(response.status_code, 200)
        self.assertIn('invalid username', str(response.content))

    def test_user_register_not_identical_passwords(self):
        """
        Error if username has entered two different passwords.
        """

        # request with already existing user data to sign up.
        bad_name = 'dummy'
        email = 'dummy@user.com'
        password = 'dummy'
        bad_password = 'bad_dummy'
        response = self.client.post(
            '/accounts/register/',
            {'username': bad_name, 'email': email, 'password1': password, 'password2': bad_password},
        )

        # comparisons and checks.
        for st_code in [302, 301, 403, 500]:
            self.assertNotEqual(response.status_code, st_code)
        self.assertEqual(response.status_code, 200)
        self.assertIn('not identical', str(response.content))

    def test_user_register_with_unexpected_data(self):
        """
        Error when user send unexpected data via POST request.
        In this case this will be first_name and last_name.
        """
        username = 'dummy'
        email = 'dummy@user.com'

        # request with correct and complete user data to sign up.
        response = self.client.post(
            '/accounts/register/',
            {
                'username': username,
                'email': email,
                'password1': 'dummy',
                'password2': 'dummy',
                'last_name': 'John',
                'first_name': 'Doe',
            },
        )
        dummy_user = User.objects.get(username=username)

        # comparisons and checks.
        self.assertEqual(response.status_code, 302)
        self.assertIsNotNone(dummy_user)
        self.assertTrue(dummy_user.is_active)
        self.assertTrue(dummy_user.is_authenticated)
        self.assertFalse(dummy_user.is_superuser)
        self.assertFalse(dummy_user.is_staff)
        self.assertFalse(dummy_user.is_anonymous)
        self.assertEqual(dummy_user.last_name, '')
        self.assertEqual(dummy_user.first_name, '')

    def test_user_login_incorrect_username(self):
        """
        Get 404 when enter wrong nickname.
        """
        # incorrect username input gives 404 - doesn't redirect.
        response = self.client.post('/accounts/login/', {'username': 'test_wrong', 'password': 'test'})
        self.assertEqual(response.status_code, 200)

    def test_user_login_incorrect_password(self):
        """
        Get 404 when enter wrong password.
        """
        # incorrect password input gives 200 - doesn't redirect.
        response = self.client.post('/accounts/login/', {'username': 'test', 'password': 'test_wrong'})
        self.assertEqual(response.status_code, 200)

    def test_user_redirected_and_login(self):
        """
        User can log in and cannot anonymously create tasks and watch them.
        """
        # redirect unauthorized user to login page when trying to visit home page.
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)
        self.assertIn('accounts/login', response.url)

        # redirect unauthorized user to login page when trying create new task.
        response = self.client.get('/task/create/')
        self.assertEqual(response.status_code, 302)
        self.assertIn('accounts/login', response.url)

        # the user logs in and is redirected to the home page.
        response = self.client.post(response.url, {'username': 'test', 'password': 'test'})
        self.assertEqual(response.status_code, 302)

    def test_user_logout(self):
        """
        User is authenticated --> Going to '/accounts/logout/' URL --> user is not authenticated.
        """
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user log out.
        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 302)

    def test_user_profile_fields_not_required(self):
        """
        User is authenticated and empty profile -> Going to '/accounts/profile/edit/' and add nothing -> 302 to profile.
        """
        # user log in and redirect.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # add empty data (instead of old data) and save it.
        response = self.client.post('/accounts/profile/edit/', {})
        self.assertEqual(response.status_code, 302)

    def test_user_change_first_and_last_name(self):
        """
        User is authenticated and has no FNLN --> Going to '/accounts/profile/edit/' and add FNLN --> user has FNLN.
        """
        # user log in and redirect.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.user.first_name, '')
        self.assertEqual(self.user.last_name, '')

        # add first and last name.
        f_name = 'John'
        l_name = 'Doe'
        response = self.client.post('/accounts/profile/edit/', {'first_name': f_name, 'last_name': l_name})
        self.assertEqual(response.status_code, 302)

        # Update 'User' variable and do comparisons and checks.
        self.user = User.objects.get(username=self.initUsr)
        self.assertEqual(self.user.first_name, f_name)
        self.assertEqual(self.user.last_name, l_name)


class ProfileTestCase(TestCase):

    def setUp(self):
        """
        Initial create of user instance.
        """
        self.initUsr = 'test'
        self.initEmail = 'test@mail.com'
        self.initPwd = 'test'
        self.user = User.objects.create_user(username=self.initUsr, email=self.initEmail, password=self.initPwd)
        self.user.save()

    def generate_photo_file(self):
        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(file, 'png')
        file.name = 'test_pic.png'
        file.seek(0)
        return file

    def test_profile_is_created(self):
        """
        Testing if 'Profile' model instance was automatically
        created after the user was created.
        """
        profile = Profile.objects.get(user__username=self.initUsr)

        # comparisons and checks.
        self.assertIsInstance(profile, Profile)

    def test_profile_see(self):
        """
        Get 200 when user tries to see his profile.
        """
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user send GET request to see his profile page.
        url = reverse('profile')
        response = self.client.get(url)

        # comparisons and checks.
        self.assertEqual(response.status_code, 200)
        self.assertIn(self.initUsr, str(response.content))
        self.assertIn(self.initEmail, str(response.content))

    def test_profile_another_user_see(self):
        """"""
        # create new user.
        usr = 'foo'
        email = 'foo@mail.com'
        pwd = 'foo'
        self.s_user = User.objects.create_user(username=usr, email=email, password=pwd)
        self.s_user.save()

        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user send GET request to see his profile page.
        url = reverse('profile-search', kwargs={"user": usr})
        response = self.client.get(url)

        # comparisons and checks.
        self.assertEqual(response.status_code, 200)
        self.assertIn(usr, str(response.content))

    def test_profile_not_exist_see(self):
        """"""
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user send GET request to see his profile page.
        bad_name = 'nigger'
        url = reverse('profile-search', kwargs={"user": bad_name})
        response = self.client.get(url)

        # comparisons and checks.
        self.assertEqual(response.status_code, 404)

    def test_profile_fields_filling(self):
        """
        Filling of 'Profile' model fields.
        """
        profile = Profile.objects.get(user__username=self.initUsr)
        birth = date(1970, 1, 1).isoformat()
        profile.birth_date = birth
        profile.bio = 'test bio'
        profile.level = 'Trainee'
        profile.position = 'Software Developer'
        default_pic_path = '/default-user-image.png'

        # comparisons and checks.
        self.assertEqual(profile.user.email, self.initEmail)
        self.assertEqual(profile.bio, 'test bio')
        self.assertEqual(profile.birth_date, '1970-01-01')
        self.assertEqual(profile.level, Profile.UserLevel.trainee)
        self.assertEqual(profile.position, Profile.UserPosition.software_developer)
        self.assertEqual(profile.picture.name, default_pic_path, msg="\nCheck path to your default profile image.")

    def test_profile_fields_incorrect_model_filling(self):
        """
        Filling of 'Profile' model fields with incorrect data.
        """
        profile = Profile.objects.get(user__username=self.initUsr)
        profile.level = 'Trainee'
        profile.position = 'Software Developer'
        default_pic_path = '/default-user-image.png'

        # comparisons and checks.
        self.assertEqual(profile.user.email, self.initEmail)
        self.assertIn(profile.level, Profile.UserLevel)
        self.assertIn(profile.position, Profile.UserPosition)
        self.assertEqual(profile.picture.name, default_pic_path, msg="\nCheck path to your default profile image.")

    def test_profile_fields_incorrect_birth_date_filling(self):
        """
        Filling of 'Profile' model via POST with incorrect data.
        """
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user change his profile.
        bio = 'test bio'
        birth_date_future = date.today().isoformat()
        response = self.client.post('/accounts/profile/edit/', {'bio': bio, 'birth_date': birth_date_future})
        profile = Profile.objects.get(user__username=self.initUsr)

        # comparisons and checks.
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertNotEqual(profile.birth_date, birth_date_future)
        self.assertNotEqual(profile.bio, bio)

    def test_profile_fields_incorrect_POST_filling(self):
        """
        Filling of 'Profile' model via POST with incorrect data.
        """
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user change his profile.
        bio = 'test bio'
        level = 'Trainee salt'
        position = 'Software Developer salt'
        response = self.client.post('/accounts/profile/edit/', {'bio': bio, 'level': level, 'position': position})
        profile = Profile.objects.get(user__username=self.initUsr)

        # comparisons and checks.
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 302)
        self.assertNotEqual(profile.bio, bio)
        self.assertNotEqual(profile.level, level)
        self.assertNotEqual(profile.position, position)

    def test_profile_correct_birth_date_fill(self):
        """
        Filling of 'Profile' model via POST with correct data.
        """
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user change his profile.
        bio = 'test bio'
        birth_date = date.today() - timedelta(weeks=1000)
        response = self.client.post('/accounts/profile/edit/', {'bio': bio, 'birth_date': birth_date.isoformat()})
        profile = Profile.objects.get(user__username=self.initUsr)

        # comparisons and checks.
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(profile.birth_date, birth_date)
        self.assertEqual(profile.bio, bio)

    def test_profile_invalid_type_birth_date_fill(self):
        """
        Filling of 'Profile' model via POST with correct data.
        """
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user change his profile.
        bio = 'test bio'
        birth_date_invalid = 'hello'
        response = self.client.post('/accounts/profile/edit/', {'bio': bio, 'birth_date': birth_date_invalid})
        profile = Profile.objects.get(user__username=self.initUsr)

        # comparisons and checks.
        self.assertNotEqual(response.status_code, 302)
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(profile.birth_date, birth_date_invalid)
        self.assertNotEqual(profile.bio, bio)

    def test_profile_invalid_field_name(self):
        """
        Filling of 'Profile' model via POST with correct data.
        """
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        # user change his profile.
        bio = 'test bio'
        birth_date = date.today() - timedelta(weeks=1000)
        response = self.client.post('/accounts/profile/edit/', {'bio': bio, 'birth_error': birth_date})  # bad name.
        profile = Profile.objects.get(user__username=self.initUsr)

        # comparisons and checks.
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(response.status_code, 200)
        self.assertNotEqual(profile.birth_date, birth_date)
        self.assertIsNone(profile.birth_date)
        self.assertEqual(profile.bio, bio)

    def test_profile_change_picture(self):
        """"""
        # user log in.
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)

        url = reverse('profile-edit')
        photo_file = self.generate_photo_file()
        data = {'picture': photo_file}

        response = self.client.post(url, data, format='multipart')

        profile = Profile.objects.get(user__username=self.initUsr)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(profile.picture.name, 'pictures/' + photo_file.name)

        # one more time
        same_photo_file = self.generate_photo_file()
        data = {'picture': same_photo_file}

        response = self.client.post(url, data, format='multipart')

        profile = Profile.objects.get(user__username=self.initUsr)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(profile.picture.name, 'pictures/' + photo_file.name)

        # delete photo.
        pic_path = profile.picture.path
        os.remove(pic_path)

    def test_login_without_profile(self):
        """"""
        profile = Profile.objects.get(user__username=self.initUsr)
        profile.delete()
        response = self.client.post('/accounts/login/', {'username': self.initUsr, 'password': self.initPwd})
        self.assertEqual(response.status_code, 302)
