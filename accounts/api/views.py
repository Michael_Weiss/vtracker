from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from .serializers import UserSerializer


class RegisterAPIView(CreateAPIView):
    """Register new user via API."""
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]  # Or anon users can't register
    authentication_classes = []
