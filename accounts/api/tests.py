from django.urls import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User


class AccountAPITestCase(APITestCase):
    """"""

    def test_create_account_API(self):
        """"""
        name = 'test'
        pwd = 'test'
        email = 'test@test.foo'
        url = reverse('register-api')
        data = {
            'username': name,
            'password': pwd,
            'email': email
        }
        self.client.post(url, data)

        self.assertEqual(User.objects.count(), 1)
        user = User.objects.last()
        self.assertEqual(user.username, name)
        self.assertEqual(user.email, email)
