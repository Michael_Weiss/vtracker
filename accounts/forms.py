from django import forms
from datetime import date
from django.contrib.auth import get_user_model
from bootstrap_datepicker_plus import DatePickerInput

from .models import Profile

User = get_user_model()

non_allowed_usernames = ['admin', 'nigger', 'bastard', 'shit', 'asshole', 'bitch', 'cunt']


class RegisterForm(forms.Form):
    username = forms.CharField(
        label='Username', 
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    email = forms.EmailField(
        label='E-Mail', 
        widget=forms.EmailInput(attrs={'class': 'form-control'})
    )
    password1 = forms.CharField(
        label='Password', 
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )
    password2 = forms.CharField(
        label='Confirm Password',
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )

    def clean_username(self):
        username = self.cleaned_data.get("username")
        qs = User.objects.filter(username__iexact=username)
        if username in non_allowed_usernames:
            raise forms.ValidationError("This is an invalid username, please pick another.")
        if qs.exists():
            raise forms.ValidationError("This is an invalid username, please pick another.")
        return username

    def clean_email(self):
        email = self.cleaned_data.get("email")
        qs = User.objects.filter(email__iexact=email)
        if qs.exists():
            raise forms.ValidationError("This email is already in use.")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('your passwords are not identical.')
        return password2


class LoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "id": "login-input",
            }
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control",
                "id": "password-input",
            }
        )
    )

    def clean_username(self):
        username = self.cleaned_data.get("username")
        qs = User.objects.filter(username__iexact=username) # thisIsMyUsername == thisismyusername.
        if not qs.exists():
            raise forms.ValidationError("This is an invalid user.")
        return username


class ProfileModelForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = [
            'picture',
            'bio',
            'birth_date',
            'level',
            'position',
        ]
        widgets = {
            'picture': forms.FileInput(),
            'bio': forms.TextInput(attrs={'class': 'form-control'}),
            'birth_date': DatePickerInput(options={"format": "YYYY-MM-DD"}),
            'level': forms.Select(attrs={'class': 'form-control'}),
            'position': forms.Select(attrs={'class': 'form-control'}),
        }

    def clean_birth_date(self):
        birth_date = None
        try:
            birth_date = self.cleaned_data.get('birth_date')  # class 'datetime.date'
        except TypeError:
            raise forms.ValidationError("Invalid type.")  # FIXME: unnecessary exception?
        today_date = date.today()  # class 'datetime.date'
        if birth_date is not None:
            if birth_date >= today_date:
                raise forms.ValidationError("Birth date must be in the past.")
            return birth_date


class UserModelForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
        ]
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
        }
