# Generated by Django 3.1.7 on 2021-05-11 12:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20210327_1913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='picture',
            field=models.ImageField(blank=True, default='/default-user-image.png', null=True, upload_to='pictures/'),
        ),
    ]
