from django.urls import path

from .views import (
    login_view,
    logout_view,
    register_view,
    profile_edit_view,
    ProfileView,
)

urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', register_view, name='register'),
    path('profile/edit/', profile_edit_view, name='profile-edit'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('profile/<str:user>/', ProfileView.as_view(), name='profile-search'),
]