from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.conf import settings

def_pic = '/default-user-image.png'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    picture = models.ImageField(upload_to='pictures/', default=def_pic, null=True, blank=True)
    bio = models.TextField(max_length=200, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    class UserLevel(models.TextChoices):
        trainee = 'Trainee'
        junior = 'Junior'
        middle = 'Middle'
        senior = 'Senior'
        lead = 'Lead'

    class UserPosition(models.TextChoices):
        network_administrator = 'Network Administrator'
        software_developer = 'Software Developer'
        software_tester = 'Software Tester'
        engineer = 'Engineer'
        security_analyst = 'Security Analyst'
        database_engineer = 'Systems/Database Engineer'
        other = 'other'

    position = models.CharField(max_length=200, choices=UserPosition.choices, default=UserPosition.other, blank=True)
    level = models.CharField(max_length=200, choices=UserLevel.choices, default=UserLevel.trainee, blank=True)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()