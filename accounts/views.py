import os

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.views.generic import DetailView
from django.http import Http404
from rest_framework_simplejwt.tokens import RefreshToken

from .forms import LoginForm, RegisterForm, UserModelForm, ProfileModelForm
from .models import Profile

DEFAULT_PIC_URLS = [
    '/media/default-user-image.png',
    '/media/deleted-user-image.png',
    '/media/undefined-user-image.png',
]


class ProfileView(LoginRequiredMixin, DetailView):
    """Shows profile of authorized user or profile of another user if an existing name was passed in the URL."""
    model = Profile
    template_name = 'profile.html'
    login_url = '/accounts/login/'

    def get_object(self, *args, **kwargs):
        my_kwargs = self.kwargs.get('user')
        if my_kwargs is not None:
            try:
                obj = Profile.objects.get(user__username__iexact=my_kwargs)
            except ObjectDoesNotExist:
                raise Http404
        else:
            obj = Profile.objects.get(user__username__iexact=self.request.user)
        return obj


def profile_edit_view(request):
    """
    Send to user profile_edit.html page and handle POST data.
    If the user's data have been successfully updated, redirects him to his profile page.
    """
    context = {}
    if request.method == "POST":
        user_form = UserModelForm(data=request.POST, instance=request.user)
        profile_form = ProfileModelForm(data=request.POST, files=request.FILES,
                                        instance=Profile.objects.get(user_id=request.user.pk))
        success = False
        if user_form.is_valid() and profile_form.is_valid():
            usr = user_form.save(commit=False)
            profile = profile_form.save(commit=False)
            if 'picture' in profile_form.changed_data:
                init_pic = profile_form.initial['picture'].url
                if init_pic not in DEFAULT_PIC_URLS:
                    old_pic_path = profile_form.initial['picture'].path
                    os.remove(old_pic_path)
            usr.save()
            profile.save()
            success = True
        if success:
            return redirect('profile')
    else:
        user_form = UserModelForm(instance=request.user)
        profile_form = ProfileModelForm(instance=Profile.objects.get(user_id=request.user.pk))

    context['user_form'] = user_form
    context['profile_form'] = profile_form
    return render(request, 'profile_edit.html', context)


def login_view(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            try:
                user.profile
            except ObjectDoesNotExist:
                # crate 'Profile' instance if the user does not have.
                user = Profile.objects.create(user=User.objects.get(username=username))
            try:
                login(request, user)
            except:  # FIXME
                login(request, user.user)
            return redirect("/")
        else:
            request.session['invalid_user'] = 1  # 1 == True.
    return render(request, "login_forms.html", {"form": form, "authenticated": request.user.is_authenticated})


def register_view(request):
    form = RegisterForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        email = form.cleaned_data.get("email")
        password = form.cleaned_data.get("password1")
        try:  # FIXME
            user = User.objects.create_user(username, email, password)
        except:
            user = None
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            request.session['register_error'] = 1  # 1 == True
    return render(request, "register_forms.html", {"form": form, "authenticated": request.user.is_authenticated})


def logout_view(request):
    logout(request)
    # TODO: if user has no 'refresh-token' in COOKIES - don't blacklist anything.
    if 'refresh-token' in request.COOKIES:
        try:
            token = RefreshToken(request.COOKIES['refresh-token'])
            token.blacklist()
        except Exception:
            pass
    # request.user == Anon User.
    return redirect('/')
